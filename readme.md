Build Platform   | Status
---------------- | ----------------------
Linux x64        | ![Linux x64](https://img.shields.io/bitbucket/pipelines/grottansbarn/learning.svg?style=flat-square)
                 | ![Linux x64](https://img.shields.io/docker/automated/grottansbarn/learning.svg?style=flat-square)
                 
Learning repository.
Game engine with SDL and OpenGl ES2.


Build:
cmake .
make

Copy executable from build folder into the root folder of the project.
