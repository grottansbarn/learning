#include "functions.hxx"

#include "chrono"
#include "ctime"
#include <random>

#define ADDCOLUMNS // comment out for light play without adding columns
#define SHIFTING // comment out for turn-OFF shifting in left
#define ADDROW // comment out for light play in extreme mode

float OFFSET = 2.0f;

extern size_t WIDTH;
extern size_t HEIGHT;
//extern size_t BLOCK;

type TYPE = type::classic;
mode MODE = mode::select_type_game;
mode MODE_copy = mode::select_type_game;

size_t flip_counter = 0;
size_t adding_counter = 0;
size_t falling_counter = 0;
size_t shifthing_counter = 0;

class RandomGenerator {
public:
    static std::mt19937& getMt19937();

private:
    RandomGenerator()
    {
        std::random_device rd;

        if (rd.entropy() != 0) {
            std::seed_seq seed{ rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd() };
            mMt.seed(seed);
        } else {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            mMt.seed(seed);
        }
    }
    ~RandomGenerator() {}
    static RandomGenerator& instance()
    {
        static RandomGenerator s;
        return s;
    }
    RandomGenerator(RandomGenerator const&) = delete;
    RandomGenerator& operator=(RandomGenerator const&) = delete;

    std::mt19937 mMt;
};
std::mt19937& RandomGenerator::getMt19937()
{
    return RandomGenerator::instance().mMt;
}

static std::mt19937& mt = RandomGenerator::getMt19937();
static std::uniform_real_distribution<double> dist_1_5(10.0, 60.0);
static std::uniform_real_distribution<double> dist_1_17(10.0, 175.0);
static std::uniform_real_distribution<double> dist_1_11(10.0, 120.0);

///////////////////////////////////////////////////////////////////////////////
/// \brief select, searching matches AND selecting them
///
bool select_around(std::array<Block*, NUM_OF_BLOCKS>& MAP, const size_t& i, const size_t& j)
{
    // i - line, j - row
    size_t index = i * 10 + j;
    bool result = false; // if search find nothing

    if (MAP.at(index)->color == palette::black) {
        MAP[index]->selected = false;
        return result;
    }

    if (MAP.at(index)->color == palette::bomb) {
        MAP.at(i * 10 + j)->selected = true;
        if (j > 0)
            MAP.at(i * 10 + j - 1)->selected = true;
        if (j < 9)
            MAP.at(i * 10 + j + 1)->selected = true;

        if (i > 0) {
            MAP.at((i - 1) * 10 + j)->selected = true;
            if (j < 9)
                MAP.at((i - 1) * 10 + j + 1)->selected = true;
            if (j > 0)
                MAP.at((i - 1) * 10 + j - 1)->selected = true;
        }

        if (i < 9) {
            MAP.at((i + 1) * 10 + j)->selected = true;
            if (j > 0)
                MAP.at((i + 1) * 10 + j - 1)->selected = true;
            if (j < 9)
                MAP.at((i + 1) * 10 + j + 1)->selected = true;
        }
        return true;
    }

    // gorizontal search
    if (j < 9) { // search right
        if (MAP[index + 1]->color == MAP[index]->color && MAP[index + 1]->visible == true) {
            result = true;
            MAP[index + 1]->selected = true;
        }
    }

    if (j > 0) { // search left
        if (MAP[index - 1]->color == MAP[index]->color && MAP[index - 1]->visible == true) {
            result = true;
            MAP[index - 1]->selected = true;
        }
    }
    // vertical search
    if (i < 9) { // search down
        if (MAP[(i + 1) * 10 + j]->color == MAP[index]->color && MAP[(i + 1) * 10 + j]->visible == true) {
            result = true;
            MAP[(i + 1) * 10 + j]->selected = true;
        }
    }
    if (i > 0) { // search up
        if (MAP[(i - 1) * 10 + j]->color == MAP[index]->color && MAP[(i - 1) * 10 + j]->visible == true) {
            result = true;
            MAP[(i - 1) * 10 + j]->selected = true;
        }
    }
    return result; // if block can be select
}

///////////////////////////////////////////////////////////////////////////////
/// \brief canSelect, searching matches but NOT selecting them
///
bool can_select(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const size_t& i, const size_t& j)
{
    // i - line, j - row
    size_t index = i * 10 + j;
    bool result = false; // if search find nothing

    // if block Black - return false
    if (MAP[index]->color == palette::black)
        return result;

    // gorizontal search
    if (j < 9) { // search right
        if (MAP[index + 1]->color != palette::black || MAP[index + 1]->color != palette::bomb) {
            if (MAP[index + 1]->color == MAP[index]->color && MAP[index + 1]->visible == true) {
                result = true;
            }
        }
    }
    if (j > 0) { // search left
        if (MAP[index - 1]->color != palette::black || MAP[index - 1]->color != palette::bomb) {
            if (MAP[index - 1]->color == MAP[index]->color && MAP[index - 1]->visible == true) {
                result = true;
            }
        }
    }
    // vertical search
    if (i < 9) { // search down
        if (MAP[(i + 1) * 10 + j]->color != palette::black || MAP[(i + 1) * 10 + j]->color != palette::bomb) {
            if (MAP[(i + 1) * 10 + j]->color == MAP[index]->color && MAP[(i + 1) * 10 + j]->visible == true) {
                result = true;
            }
        }
    }
    if (i > 0) { // search up
        if (MAP[(i - 1) * 10 + j]->color != palette::black || MAP[(i - 1) * 10 + j]->color != palette::bomb) {
            if (MAP[(i - 1) * 10 + j]->color == MAP[index]->color && MAP[(i - 1) * 10 + j]->visible == true) {
                result = true;
            }
        }
    }
    return result; // if block can be select
}

///////////////////////////////////////////////////////////////////////////////
/// \brief selecting blocks all blocks
///
size_t selecting(std::array<Block*, NUM_OF_BLOCKS>& MAP)
{
    size_t number_of_selected_blocks = 0;

    for (size_t j = 0; j < 10; j++) {
        for (size_t i = 0; i < NUM_OF_BLOCKS; i++) {
            if (MAP[i]->selected) {
                select_around(MAP, i / 10, i % 10);
            }
        }
    }
    // counting numberOfSelectedBlocks
    for (size_t i = 0; i < 100; i++)
        if (MAP[i]->selected) {
            number_of_selected_blocks++;
        }
    //    std::cout << "Selected " << numberOfSelectedBlocks << " blocks\n"
    //              << std::endl;
    return number_of_selected_blocks;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief unselect_all
///
void unselect_all(std::array<Block*, NUM_OF_BLOCKS>& MAP_)
{
    for (Block* x : MAP_) {
        x->selected = false;
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief unmotion_all
///
void motion_reset(std::array<Block*, NUM_OF_BLOCKS>& MAP_)
{
    for (Block* x : MAP_) {
        x->motion = false;
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief flip_blocks
///
void replace_blocks(std::array<Block*, NUM_OF_BLOCKS>& MAP_,
    const size_t& i, const size_t& j, const size_t& m, const size_t& n)
{
    Block copy{};

    copy.color = MAP_.at(i * 10 + j)->color;
    copy.texture = MAP_.at(i * 10 + j)->texture;
    copy.selected = MAP_.at(i * 10 + j)->selected;
    copy.visible = MAP_.at(i * 10 + j)->visible;

    MAP_.at(i * 10 + j)->color = MAP_.at(m * 10 + n)->color;
    MAP_.at(i * 10 + j)->texture = MAP_.at(m * 10 + n)->texture;
    MAP_.at(i * 10 + j)->selected = MAP_.at(m * 10 + n)->selected;
    MAP_.at(i * 10 + j)->visible = MAP_.at(m * 10 + n)->visible;

    MAP_.at(m * 10 + n)->color = copy.color;
    MAP_.at(m * 10 + n)->texture = copy.texture;
    MAP_.at(m * 10 + n)->selected = copy.selected;
    MAP_.at(m * 10 + n)->visible = copy.visible;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief replace_blocks_with_animation
///
void replace_blocks_with_animation(size_t half, std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const std::array<grottans::vertex_buffer*, 16>& v_falling_buffers,
    const size_t& m, const size_t& n, direction dir)
{
    // bool half = true if we need to flip blocks on 180deg, false - if 360deg
    // blocking work in other modes
    if (MODE != mode::flip)
        return;

    if (dir == direction::left) {
        if (n == 0 || MAP[m * 10 + n - 1]->color == palette::black) { // blocking left margin and black
            MODE = mode::game_extreme;
            return;
        }
        if (flip_counter == 0 || flip_counter == 4) {
            MAP[m * 10 + n]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n]->position.x -= OFFSET / 4;
            MAP[m * 10 + n - 1]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->position.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 1 || flip_counter == 5) {
            MAP[m * 10 + n]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n]->position.x -= OFFSET / 4;
            MAP[m * 10 + n - 1]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->position.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->v_buf = v_falling_buffers[0];
            flip_counter++;
            return;
        }
        if (flip_counter == 2 || flip_counter == 6) {
            MAP[m * 10 + n]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n]->position.x -= OFFSET / 4;
            MAP[m * 10 + n - 1]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->position.x += OFFSET / 4;
            MAP[m * 10 + n - 1]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 3 || flip_counter == 7) {
            // restoring previous states of blocks and replacing them
            MAP[m * 10 + n]->move.delta.x += OFFSET / 4 * 3;
            MAP[m * 10 + n]->position.x += OFFSET / 4 * 3;
            MAP[m * 10 + n - 1]->move.delta.x -= OFFSET / 4 * 3;
            MAP[m * 10 + n - 1]->position.x -= OFFSET / 4 * 3;
            MAP[m * 10 + n - 1]->v_buf = v_falling_buffers[15];
            replace_blocks(MAP, m, n, m, n - 1);
        }
    }

    if (dir == direction::right) {
        if (n == 9 || MAP[m * 10 + n + 1]->color == palette::black) { // blocking right margin
            MODE = mode::game_extreme;
            return;
        }
        if (flip_counter == 0 || flip_counter == 4) {
            MAP[m * 10 + n]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n]->position.x += OFFSET / 4;
            MAP[m * 10 + n + 1]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->position.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 1 || flip_counter == 5) {
            MAP[m * 10 + n]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n]->position.x += OFFSET / 4;
            MAP[m * 10 + n + 1]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->position.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->v_buf = v_falling_buffers[0];
            flip_counter++;
            return;
        }
        if (flip_counter == 2 || flip_counter == 6) {
            MAP[m * 10 + n]->move.delta.x += OFFSET / 4;
            MAP[m * 10 + n]->position.x += OFFSET / 4;
            MAP[m * 10 + n + 1]->move.delta.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->position.x -= OFFSET / 4;
            MAP[m * 10 + n + 1]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 3 || flip_counter == 7) {
            // restoring previous states of blocks and replacing them
            MAP[m * 10 + n]->move.delta.x -= OFFSET / 4 * 3;
            MAP[m * 10 + n]->position.x -= OFFSET / 4 * 3;
            MAP[m * 10 + n + 1]->move.delta.x += OFFSET / 4 * 3;
            MAP[m * 10 + n + 1]->position.x += OFFSET / 4 * 3;
            MAP[m * 10 + n + 1]->v_buf = v_falling_buffers[15];
            replace_blocks(MAP, m, n, m, n + 1);
        }
    }

    if (dir == direction::up) {
        if (m == 0 || MAP[(m - 1) * 10 + n]->color == palette::black) { //blocking up margin
            MODE = mode::game_extreme;
            return;
        }
        if (flip_counter == 0 || flip_counter == 4) {
            MAP[m * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[m * 10 + n]->position.y += OFFSET / 4;
            MAP[(m - 1) * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 1 || flip_counter == 5) {
            MAP[m * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[m * 10 + n]->position.y += OFFSET / 4;
            MAP[(m - 1) * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->v_buf = v_falling_buffers[0];
            flip_counter++;
            return;
        }
        if (flip_counter == 2 || flip_counter == 6) {
            MAP[m * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[m * 10 + n]->position.y += OFFSET / 4;
            MAP[(m - 1) * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m - 1) * 10 + n]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 3 || flip_counter == 7) {
            MAP[m * 10 + n]->move.delta.y -= OFFSET / 4 * 3;
            MAP[m * 10 + n]->position.y -= OFFSET / 4 * 3;
            MAP[(m - 1) * 10 + n]->move.delta.y += OFFSET / 4 * 3;
            MAP[(m - 1) * 10 + n]->position.y += OFFSET / 4 * 3;
            MAP[(m - 1) * 10 + n]->v_buf = v_falling_buffers[15];
            replace_blocks(MAP, m, n, m - 1, n);
        }
    }

    if (dir == direction::down) {
        if (m == 9 || MAP[(m + 1) * 10 + n]->color == palette::black) { //blocking up margin
            MODE = mode::game_extreme;
            return;
        }
        if (flip_counter == 0 || flip_counter == 4) {
            MAP[m * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[m * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m + 1) * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->position.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 1 || flip_counter == 5) {
            MAP[m * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[m * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m + 1) * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->position.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->v_buf = v_falling_buffers[0];
            flip_counter++;
            return;
        }
        if (flip_counter == 2 || flip_counter == 6) {
            MAP[m * 10 + n]->move.delta.y -= OFFSET / 4;
            MAP[m * 10 + n]->position.y -= OFFSET / 4;
            MAP[(m + 1) * 10 + n]->move.delta.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->position.y += OFFSET / 4;
            MAP[(m + 1) * 10 + n]->v_buf = v_falling_buffers[7];
            flip_counter++;
            return;
        }
        if (flip_counter == 3 || flip_counter == 7) {
            MAP[m * 10 + n]->move.delta.y += OFFSET / 4 * 3;
            MAP[m * 10 + n]->position.y += OFFSET / 4 * 3;
            MAP[(m + 1) * 10 + n]->move.delta.y -= OFFSET / 4 * 3;
            MAP[(m + 1) * 10 + n]->position.y -= OFFSET / 4 * 3;
            MAP[(m + 1) * 10 + n]->v_buf = v_falling_buffers[15];
            replace_blocks(MAP, m, n, m + 1, n);
        }
    }

    // the end
    if (half == 3) {
        if (flip_counter == 3) {
            MODE = mode::game_extreme;
            flip_counter = 0;
        }
    }
    if (half == 7) {
        if (flip_counter == 3) {
            flip_counter++;
        }
        if (flip_counter == 7) {
            MODE = mode::game_extreme;
            flip_counter = 0;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief can_flip
///
bool can_flip(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const size_t& i, const size_t& j, direction dir)
{
    // if checking pair block-to-bomb -> always true, because bomb selecting return 9 !!!

    bool result = false; // if search find nothing

    // i - line, j - row
    size_t index = i * 10 + j;

    // if block Black - return false
    if (MAP[index]->color == palette::black)
        return result;
    if (MAP[index]->color == palette::bomb)
        return result;

    // sort by direction
    if (dir == direction::up) {
        if (i > 0) {
            if (MAP.at((i - 1) * 10 + j)->color != palette::black) {

                if (MAP.at((i - 1) * 10 + j)->color == palette::bomb) {
                    replace_blocks(MAP, i, j, i - 1, j);
                    MAP.at((i - 1) * 10 + j)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                    unselect_all(MAP);
                    replace_blocks(MAP, i - 1, j, i, j);
                    return result;
                }

                replace_blocks(MAP, i, j, i - 1, j);
                MAP.at(i * 10 + j)->selected = true;
                if (selecting(MAP) >= 3) {
                    unselect_all(MAP);
                    result = true;
                } else {
                    unselect_all(MAP);
                    MAP.at((i - 1) * 10 + j)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                }
                //restore previous MAP state
                unselect_all(MAP);
                replace_blocks(MAP, i - 1, j, i, j);
            }
        }
    }
    if (dir == direction::down) {
        if (i < 9) {
            if (MAP.at((i + 1) * 10 + j)->color != palette::black) {
                if (MAP.at((i + 1) * 10 + j)->color == palette::bomb) {
                    replace_blocks(MAP, i, j, i + 1, j);
                    MAP.at((i + 1) * 10 + j)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                    unselect_all(MAP);
                    replace_blocks(MAP, i + 1, j, i, j);
                    return result;
                }
                replace_blocks(MAP, i, j, i + 1, j);
                MAP.at(i * 10 + j)->selected = true;
                if (selecting(MAP) >= 3) {
                    unselect_all(MAP);
                    result = true;
                } else {
                    unselect_all(MAP);
                    MAP.at((i + 1) * 10 + j)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                }
                //restore previous MAP state
                unselect_all(MAP);
                replace_blocks(MAP, i + 1, j, i, j);
            }
        }
    }
    if (dir == direction::left) {
        if (j > 0) {
            if (MAP.at(i * 10 + j - 1)->color != palette::black) {
                if (MAP.at(i * 10 + j - 1)->color == palette::bomb) {
                    replace_blocks(MAP, i, j, i, j - 1);
                    MAP.at(i * 10 + j - 1)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                    unselect_all(MAP);
                    replace_blocks(MAP, i, j - 1, i, j);
                    return result;
                }

                replace_blocks(MAP, i, j, i, j - 1);
                MAP.at(i * 10 + j)->selected = true;
                if (selecting(MAP) >= 3) {
                    unselect_all(MAP);
                    result = true;
                } else {
                    unselect_all(MAP);
                    MAP.at(i * 10 + j - 1)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                }
                //restore previous MAP state
                unselect_all(MAP);
                replace_blocks(MAP, i, j, i, j - 1);
            }
        }
    }
    if (dir == direction::right) {
        if (j < 9) {
            if (MAP.at(i * 10 + j + 1)->color != palette::black) {

                if (MAP.at(i * 10 + j + 1)->color == palette::bomb) {
                    replace_blocks(MAP, i, j, i, j + 1);
                    MAP.at(i * 10 + j + 1)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                    unselect_all(MAP);
                    replace_blocks(MAP, i, j + 1, i, j);
                    return result;
                }

                replace_blocks(MAP, i, j, i, j + 1);
                MAP.at(i * 10 + j)->selected = true;
                if (selecting(MAP) >= 3) {
                    unselect_all(MAP);
                    result = true;
                } else {
                    unselect_all(MAP);
                    MAP.at(i * 10 + j + 1)->selected = true;
                    if (selecting(MAP) >= 3) {
                        unselect_all(MAP);
                        result = true;
                    }
                }
                //restore previous MAP state
                unselect_all(MAP);
                replace_blocks(MAP, i, j, i, j + 1);
            }
        }
    }

    return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief falling with animation
///
bool drop_animation(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const std::array<grottans::vertex_buffer*, 16>& v_falling_buffers)
{
    bool result = false;

    // blocking work in shifting mode
    if (MODE == mode::shifting || MODE == mode::level_complite || MODE == mode::new_level || MODE == mode::adding)
        return false;

    //saving previous mode
    //    if (MODE != mode::falling && falling_counter == 0) {
    //        MODE_copy = MODE;
    //    }

    for (size_t j = 0; j < 10; j++) {
        for (size_t i = 0; i < 10; i++) {
            // if block not disappearing
            if (MAP.at(i * 10 + j)->selected == true) {
                //then turn-on falling mode
                size_t index = i * 10 + j;

                result = true;

                switch (falling_counter) {
                case 1: {
                    MAP.at(index)->v_buf = v_falling_buffers[13];
                    break;
                }
                case 2: {
                    MAP.at(index)->v_buf = v_falling_buffers[12];
                    break;
                }
                case 3: {
                    MAP.at(index)->v_buf = v_falling_buffers[11];
                    break;
                }
                case 4: {
                    MAP.at(index)->v_buf = v_falling_buffers[10];
                    break;
                }
                case 5: {
                    MAP.at(index)->v_buf = v_falling_buffers[9];
                    break;
                }
                case 6: {
                    MAP.at(index)->v_buf = v_falling_buffers[8];
                    break;
                }
                case 7: {
                    MAP.at(index)->v_buf = v_falling_buffers[7];
                    break;
                }
                case 8: {
                    MAP.at(index)->v_buf = v_falling_buffers[6];
                    break;
                }
                case 9: {
                    MAP.at(index)->v_buf = v_falling_buffers[5];
                    break;
                }
                case 10: {
                    MAP.at(index)->v_buf = v_falling_buffers[4];
                    break;
                }
                case 11: {
                    MAP.at(index)->v_buf = v_falling_buffers[3];
                    break;
                }
                case 12: {
                    MAP.at(index)->v_buf = v_falling_buffers[2];
                    break;
                }
                case 13: {
                    MAP.at(index)->v_buf = v_falling_buffers[1];
                    break;
                }
                case 14: {
                    MAP.at(index)->v_buf = v_falling_buffers[0];
                    break;
                }
                case 15: {
                    MAP.at(index)->v_buf = v_falling_buffers[15];
                    MAP.at(index)->visible = false;
                    break;
                }
                }
            }
        }
    }

    // moving blocks down on OFFSET/24
    if (falling_counter) {
        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 10; j++) {
                if (MAP.at(i * 10 + j)->selected == true) {

                    //std::cout << "OFFSET" << std::endl;

                    for (int k = i - 1; k >= 0; k--) {
                        MAP.at(k * 10 + j)->move.delta.y -= OFFSET / 24;
                        MAP.at(k * 10 + j)->position.y -= OFFSET / 24;

                        //std::cout << k << ' ' << j << std::endl;
                    }
                }
            }
        }
    }

    // restoring original values of blocks positions
    if (falling_counter == 15) {
        for (int i = 9; i >= 0; i--) {
            for (int j = 0; j < 10; j++) {
                if (MAP.at(i * 10 + j)->selected == true) {

                    for (int k = i - 1; k >= 0; k--) {
                        MAP[k * 10 + j]->move.delta.y = 0;
                        MAP[k * 10 + j]->position.y = 10.f - k * 2;
                    }
                }
            }
        }
    }

    if (result) {
        falling_counter++;
        //std::cout << falling_counter << std::endl;
    }

    if (falling_counter) {
        // switch mode to falling if need
        MODE = mode::falling;
    }

    // replace blocks
    if (falling_counter == 16) {
        //move blocks down
        for (size_t k = 0; k < 9; k++) {
            for (size_t j = 0; j < 10; j++) {
                for (size_t i = 0; i < 9; i++) {
                    // if block unselected
                    if (MAP.at(i * 10 + j)->selected == false) {
                        // and lower block selected
                        if (MAP.at((i + 1) * 10 + j)->selected) {

                            // assing under block parameters of high block
                            // except position
                            MAP.at((i + 1) * 10 + j)->color = MAP.at(i * 10 + j)->color;
                            MAP.at((i + 1) * 10 + j)->texture = MAP.at(i * 10 + j)->texture;
                            MAP.at((i + 1) * 10 + j)->selected = MAP.at(i * 10 + j)->selected;
                            MAP.at((i + 1) * 10 + j)->visible = MAP.at(i * 10 + j)->visible;

                            MAP.at(i * 10 + j)->color = palette::non;
                            MAP.at(i * 10 + j)->visible = false;
                            MAP.at(i * 10 + j)->selected = true;
                        }
                    }
                }
            }
        }

        // update high row of the blocks
        for (size_t i = 0; i < 10; i++) {
            if (MAP[i]->selected || MAP[i]->visible == false) {
                MAP.at(i)->color = palette::non;
                MAP.at(i)->visible = false;
                MAP.at(i)->selected = true;
            }
        }
        // update low row of the blocks
        for (size_t i = 90; i < 100; i++) {
            if (MAP[i]->selected || MAP[i]->visible == false) {
                MAP.at(i)->color = palette::non;
                MAP.at(i)->visible = false;
                MAP.at(i)->selected = true;
            }
        }

        //backup previous mode
        //MODE = MODE_copy;

        if (TYPE == type::classic) {
            MODE = mode::game_classic;
        } else {
            MODE = mode::game_extreme;
        }

        //update counter
        falling_counter = 0;
    }

    return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief isGameOver - checking all MAP on selectable blocks
///
bool is_game_over(std::array<Block*, NUM_OF_BLOCKS>& MAP, const type type_)
{
    bool result = true;
    if (type_ == type::classic) {
        for (size_t i = 0; i < 10; i++) {
            for (size_t j = 0; j < 10; j++) {
                if (can_select(MAP, i, j))
                    result = false;
            }
        }
    }

    if (type_ == type::extreme) {
        for (size_t i = 0; i < 10; i++) {
            for (size_t j = 0; j < 10; j++) {
                if (can_flip(MAP, i, j, direction::up)) {
                    result = false;
                }
                if (can_flip(MAP, i, j, direction::down)) {
                    result = false;
                }
                if (can_flip(MAP, i, j, direction::left)) {
                    result = false;
                }
                if (can_flip(MAP, i, j, direction::right)) {
                    result = false;
                }
            }
        }
    }
    // if game over
    return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief newLevel - generating new feild
///
void new_level(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture* const& tex_yellow,
    grottans::texture* const& tex_purple,
    grottans::texture* const& tex_green,
    grottans::texture* const& tex_blue,
    grottans::texture* const& tex_red,
    grottans::texture* const& tex_black,
    std::array<grottans::tri2, 14>& tr,
    const type& TYPE)
{
    // reset selection block position
    MAP[100]->position = { OFFSET / 2, 0.f };

    // reset the length of the counter line to NULL  -0.91
    tr[4].v[1].pos.x = -0.91f;
    tr[4].v[2].pos.x = -0.91f;
    tr[5].v[1].pos.x = -0.91f;

    // correction SCORE if points from previous level were large
    //SCORE = levels[level_number - 1];
    //SCORE = SCORE_old_level;

    // creating 0..99 random blocks
    //srand (time(NULL));
    for (size_t i = 0; i < 10; i++) {
        for (size_t j = 0; j < 10; j++) {
            size_t index = i * 10 + j;

            MAP[index]->visible = true;
            MAP[index]->selected = false;

            if (TYPE == type::classic) {
                // random number generator
                int m;
                //srand(static_cast<int>(clock()));
                //srand (time(NULL));
                //std::chrono::system_clock::now()
                //m = 0 + rand() % 5;
                m = static_cast<int>(dist_1_5(mt) / 10);

                switch (m) {
                case 1:
                    MAP[index]->texture = tex_yellow;
                    MAP[index]->color = palette::yellow;
                    break;
                case 2:
                    MAP[index]->texture = tex_green;
                    MAP[index]->color = palette::green;
                    break;
                case 3:
                    MAP[index]->texture = tex_red;
                    MAP[index]->color = palette::red;
                    break;
                case 4:
                    MAP[index]->texture = tex_blue;
                    MAP[index]->color = palette::blue;
                    break;
                default:
                    MAP[index]->texture = tex_purple;
                    MAP[index]->color = palette::purple;
                    break;
                }
            }
            if (TYPE == type::extreme) {
                // random number generator
                int m;
                //srand (time(NULL));
                //srand(static_cast<int>(clock()));
                //m = 0 + rand() % 11;
                m = static_cast<int>(dist_1_11(mt) / 10);

                if (m == 1 || m == 6) {
                    MAP[index]->texture = tex_yellow;
                    MAP[index]->color = palette::yellow;
                }
                if (m == 2 || m == 7) {
                    MAP[index]->texture = tex_green;
                    MAP[index]->color = palette::green;
                }
                if (m == 3 || m == 8) {
                    MAP[index]->texture = tex_red;
                    MAP[index]->color = palette::red;
                }
                if (m == 4 || m == 9) {
                    MAP[index]->texture = tex_blue;
                    MAP[index]->color = palette::blue;
                }
                if (m == 5 || m == 10) {
                    MAP[index]->texture = tex_purple;
                    MAP[index]->color = palette::purple;
                }
                if (m == 11) {
                    MAP[index]->texture = tex_black;
                    MAP[index]->color = palette::black;
                }
            }
        }
    }
}
// }
//}

///////////////////////////////////////////////////////////////////////////////
/// \brief addColumns and shifting blocks in left
///
bool add_columns_and_shift(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture*& tex_yellow,
    grottans::texture*& tex_purple,
    grottans::texture*& tex_green,
    grottans::texture*& tex_blue,
    grottans::texture*& tex_red)
{
    bool result = false;

    if (TYPE == type::extreme)
        return result;

    // blocking work in other modes
    if (MODE != mode::shifting && MODE != mode::game_classic)
        return false;

    // backuping previous mode
    if (MODE != mode::shifting && shifthing_counter == 0) {
        MODE_copy = MODE;
    }

#ifdef ADDCOLUMNS
    // ADD A NEW COLUMNS

    //srand (time(NULL));
    if (MAP[99]->color == palette::non) {
        // create a new right row
        for (size_t k = 0; k < 10; k++) {
            size_t index = k * 10 + 9;

            MAP[index]->visible = true;

            // random number generator
            int m;
            //srand (time(NULL));
            //srand(static_cast<int>(clock()));
            //m = 0 + rand() % 5;
            m = static_cast<int>(dist_1_5(mt) / 10);

            switch (m) {
            case 1:
                MAP[index]->texture = tex_yellow;
                MAP[index]->color = palette::yellow;
                break;
            case 2:
                MAP[index]->texture = tex_green;
                MAP[index]->color = palette::green;
                break;
            case 3:
                MAP[index]->texture = tex_red;
                MAP[index]->color = palette::red;
                break;
            case 4:
                MAP[index]->texture = tex_blue;
                MAP[index]->color = palette::blue;
                break;
            default:
                MAP[index]->texture = tex_purple;
                MAP[index]->color = palette::purple;
                break;
            }
        }
    } // end of adding columns
#endif

#ifdef SHIFTING
    //moving blocks in left
    //if (shifthing_counter) { // only in shifhing mode
    for (size_t i = 0; i < 10; i++) {
        for (size_t j = 0; j < 9; j++) {
            size_t index = i * 10 + j;
            if (MAP.at(index)->visible == false && MAP.at(index + 1)->visible == true) {

                result = true;

                for (int k = j + 1; k < 10; k++) {
                    MAP.at(i * 10 + k)->move.delta.x -= OFFSET / 4;
                    MAP.at(i * 10 + k)->position.x -= OFFSET / 4;
                }
            }
        }
    }
    //}

    if (result)
        shifthing_counter++;

    if (shifthing_counter == 4) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                MAP.at(i * 10 + j)->move.delta.y = 0;
                MAP.at(i * 10 + j)->position.x = -9.f + j * 2;
            }
        }
    }

    if (shifthing_counter == 4) {
        // shift blocks in left
        for (size_t i = 0; i < 10; i++) {
            for (size_t j = 1; j < 10; j++) {

                size_t index = i * 10 + j;
                if (MAP[index - 1]->visible == false && MAP[index]->visible == true) {
                    result = true;

                    // if left block unvisible - shift
                    MAP.at(index - 1)->color = MAP.at(index)->color;
                    MAP.at(index - 1)->texture = MAP.at(index)->texture;
                    MAP.at(index - 1)->selected = MAP.at(index)->selected;
                    MAP.at(index - 1)->visible = MAP.at(index)->visible;
                    // right block - zero
                    MAP.at(index)->visible = false;
                    MAP.at(index)->selected = false;
                    MAP.at(index)->color = palette::non;
                }
            }
        } // end of shifting
    }

    if (shifthing_counter) {
        MODE = mode::shifting;
        //std::cout << "shifting " << shifthing_counter << std::endl;
    }

    // for sure
    if (shifthing_counter == 4) {
        shifthing_counter = 0;

        MODE = MODE_copy;
    }

#endif

    return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief add_top_blocks
///
bool add_top_blocks(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture*& tex_yellow,
    grottans::texture*& tex_purple,
    grottans::texture*& tex_green,
    grottans::texture*& tex_blue,
    grottans::texture*& tex_red,
    grottans::texture*& tex_black,
    grottans::texture*& tex_bomb)
{
    bool result = false;

    if (TYPE == type::classic)
        return result;

    if (MODE == mode::falling || MODE == mode::shifting || MODE == mode::select_type_game)
        return result;

#ifdef ADDROW
    //srand (time(NULL));
    for (size_t i = 0; i < 10; i++) {
        if (MAP[i]->color == palette::non) {
            //if (MAP[i]->selected || MAP[i]->visible == false) {

            //result = true;

            MAP[i]->visible = true;
            // MAP[i]->selected = false;

            // random number generator
            int m;
            //srand (time(NULL));
            //srand(static_cast<int>(clock()));
            // m = 0 + rand() % 18;
            m = static_cast<int>(dist_1_17(mt) / 10);
            // 0 - begin (include), 18 - offset (not include)

            if (m == 1 || m == 6 || m == 11) {
                MAP[i]->texture = tex_yellow;
                MAP[i]->color = palette::yellow;
            }
            if (m == 2 || m == 7 || m == 12) {
                MAP[i]->texture = tex_green;
                MAP[i]->color = palette::green;
            }
            if (m == 3 || m == 8 || m == 13) {
                MAP[i]->texture = tex_red;
                MAP[i]->color = palette::red;
            }
            if (m == 4 || m == 9 || m == 14) {
                MAP[i]->texture = tex_blue;
                MAP[i]->color = palette::blue;
            }
            if (m == 5 || m == 10 || m == 15) {
                MAP[i]->texture = tex_purple;
                MAP[i]->color = palette::purple;
            }
            if (m == 16) {
                MAP[i]->texture = tex_black;
                MAP[i]->color = palette::black;
            }
            if (m == 17) {
                MAP[i]->texture = tex_bomb;
                MAP[i]->color = palette::bomb;
            }
        }
    }

    //moving blocks down
    for (size_t j = 0; j < 10; j++) {
        for (size_t i = 9; i > 0; i--) {

            size_t index = i * 10 + j;

            if (MAP.at(index)->visible == false && MAP.at((i - 1) * 10 + j)->visible == true) {

                result = true;

                for (int k = i - 1; k >= 0; k--) {
                    MAP.at(k * 10 + j)->move.delta.y -= OFFSET / 4;
                    MAP.at(k * 10 + j)->position.y -= OFFSET / 4;
                }
            }
        }
    }

    if (result)
        adding_counter++;

    if (adding_counter == 4) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                MAP.at(i * 10 + j)->move.delta.y = 0;
                MAP.at(i * 10 + j)->position.y = 10.f - i * 2;
            }
        }
    }

    if (adding_counter == 4) {
        // move blocks down
        for (size_t i = 0; i < 9; i++) {
            for (size_t j = 0; j < 10; j++) {

                size_t index = i * 10 + j;
                if (MAP[(i + 1) * 10 + j]->visible == false && MAP[index]->visible == true) {
                    result = true;

                    // i block unvisible - down
                    MAP.at((i + 1) * 10 + j)->color = MAP.at(index)->color;
                    MAP.at((i + 1) * 10 + j)->texture = MAP.at(index)->texture;
                    MAP.at((i + 1) * 10 + j)->selected = MAP.at(index)->selected;
                    MAP.at((i + 1) * 10 + j)->visible = MAP.at(index)->visible;
                    // right block - zero
                    MAP.at(index)->visible = false;
                    MAP.at(index)->selected = false;
                    MAP.at(index)->color = palette::non;
                }
            }
        } // end of shifting
    }

    if (adding_counter) {
        MODE = mode::adding;
        //std::cout << "adding counter " << adding_counter << std::endl;
    }

    // for sure
    if (adding_counter == 4) {
        adding_counter = 0;
        MODE = mode::game_extreme;
    }
#endif
    return result;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief delta to points - translate delta to points
///
size_t delta_to_points(size_t delta, std::array<size_t, 25> points,
    std::array<size_t, 30> points_extreme, type t)
{
    if (t == type::classic) {
        if (delta < 24)
            return points[delta - 2];
        else
            return points[24];
    }
    if (t == type::extreme) {
        if (delta < 29)
            return points_extreme[delta - 3];
        else
            return points_extreme[29];
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief popup_points if selecting succsses
///
void popup_points(grottans::Engine*& eng, const size_t delta, const float x, const float y)
{
    //x - MAP[100].position.x
    //y - MAP[100].position.y
    // blocking work in another modes
    //if (MODE == mode::falling) {

    //    MAP[i]->move.delta.x = WIDTH / 240 * MAP[i]->position.x;
    //    MAP[i]->move.delta.y = HEIGTH / 268 * MAP[i]->position.y;
    //    grottans::mat2x3 m_MAP = MAP[i]->move * MAP[i]->aspect;
    //    eng->render(*MAP[i]->v_buf, MAP[i]->texture, m_MAP);
    //}
    return;
}
