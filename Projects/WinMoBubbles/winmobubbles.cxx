#include <array>
#include <charconv>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>

#include "engine.hxx"
#include "functions.hxx"

//#define WIDTH 480.f // 640.f // 480.f //original 240.f
//#define HEIGTH 536.f // 715.f //536.f //original 268.f
//#define BLOCK 24.f // original 24.f

//size_t WIDTH = 0;
//size_t HEIGHT = 0;
//size_t BLOCK = 0;

#define NUM_OF_BLOCKS 101

using clock_timer = std::chrono::high_resolution_clock;
using nano_sec = std::chrono::nanoseconds;
using milli_sec = std::chrono::milliseconds;
using time_point = std::chrono::time_point<clock_timer, nano_sec>;

static bool sound = true;
static size_t SCORE = 0;
static size_t DELTA_SCORE = 0;
static size_t level_number = 1;
static size_t score_in_the_end_of_level = 0;

///////////////////////////////////////////////////////////////////////////////
/// \brief MAIN
///
int main()
{
    bool START = false;
    // variables for mouse handling in extreme mode (for blocks pulling)
    int mouse_x_pressed = 0;
    int mouse_y_pressed = 0;
    int mouse_x_released = 0;
    int mouse_y_released = 0;
    //for fliping mode
    size_t m = 0;
    size_t n = 0;
    direction flip = direction::non;
    size_t half = 3;

    size_t first_time_click = 0; // handle false clicks

    std::array<size_t, 25> points = { 2, 6, 12, 20, 30, 42, 56, 72, 90,
        110, 132, 156, 182, 210, 240, 272, 306, 342,
        382, 424, 468, 514, 562, 750, 999 };

    std::array<size_t, 30> points_extreme = { 3, 8, 15, 24, 35, 48, 63,
        80, 99, 120, 143, 168, 195, 224, 255, 288, 323, 360, 399,
        440, 483, 528, 575, 624, 675, 728, 783, 840, 899, 999 };

    size_t delta_points = 0;

    std::array<size_t, 26> levels = { 0, 200, 225, 250, 275, 300, 325,
        350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675,
        700, 725, 750, 775, 800 };

    std::array<size_t, 26> levels_extreme = { 0, 800, 1100, 1400, 1700, 2000,
        2300, 2600, 2900, 3200, 3500, 3800, 4100, 4400, 4700, 5000, 5200, 5400,
        5600, 5800, 6000, 6200, 6400, 6600, 6800, 7000 };

    std::array<Block*, NUM_OF_BLOCKS> MAP; //field
    std::array<grottans::tri2, 14> tr; // v_buf triangles
    std::array<grottans::tri2, 10> tr_score;
    std::array<grottans::tri2, 8> tr_points;
    std::array<grottans::tri2, 2> tr_falling;
    std::array<size_t, 5> numbers_score; //for 5 numbers of the score
    std::array<size_t, 4> numbers_points; //for 4 numbers of the points

    grottans::Engine* enginereal = grottans::create_engine(); // GAME ENGINE

    enginereal->initialize();

    size_t WIDTH = enginereal->get_window_width();
    size_t HEIGHT = enginereal->get_window_height();
    size_t BLOCK = static_cast<size_t>(WIDTH / 10.);

    // loading blocks textures
    grottans::texture* tex_selected_move = enginereal->createTexture("./images/selected_origin_move.png");
    grottans::texture* tex_bomb = enginereal->createTexture("./images/animated/bomb128x2048 (copy).png");
    grottans::texture* tex_yellow = enginereal->createTexture("./images/animated/yellow128x2048_2.png");
    grottans::texture* tex_purple = enginereal->createTexture("./images/animated/purple128x2048_2.png");
    grottans::texture* tex_green = enginereal->createTexture("./images/animated/green128x2048_2.png");
    grottans::texture* tex_black = enginereal->createTexture("./images/animated/black128x2048.png");
    grottans::texture* tex_blue = enginereal->createTexture("./images/animated/blue128x2048_2.png");
    grottans::texture* tex_red = enginereal->createTexture("./images/animated/red128x2048_2.png");
    grottans::texture* tex_selected = enginereal->createTexture("./images/selected_origin.png");
    // loading game textures
    grottans::texture* tex_select_game_type_no_sound = enginereal->createTexture("./images/my/select_game_type_no_sound.png");
    grottans::texture* tex_counter_empty = enginereal->createTexture("./images/my/counter_empty_square.png");
    grottans::texture* tex_select_game_type = enginereal->createTexture("./images/my/select_game_type.png");
    grottans::texture* tex_counter_full = enginereal->createTexture("./images/my/counter_line_square.png");
    grottans::texture* tex_level_yellow = enginereal->createTexture("./images/my/level_yellow.png");
    grottans::texture* tex_classic = enginereal->createTexture("./images/my/main_select.png");
    grottans::texture* tex_extreme = enginereal->createTexture("./images/my/main_select.png");
    grottans::texture* tex_game_over = enginereal->createTexture("./images/my/game_over.png");
    grottans::texture* tex_level_red = enginereal->createTexture("./images/my/level_red.png");

    // loadinf numbers textures
    std::array<grottans::texture*, 11> numbers;
    numbers[0] = enginereal->createTexture("./images/my_font/0.png");
    numbers[1] = enginereal->createTexture("./images/my_font/1.png");
    numbers[2] = enginereal->createTexture("./images/my_font/2.png");
    numbers[3] = enginereal->createTexture("./images/my_font/3.png");
    numbers[4] = enginereal->createTexture("./images/my_font/4.png");
    numbers[5] = enginereal->createTexture("./images/my_font/5.png");
    numbers[6] = enginereal->createTexture("./images/my_font/6.png");
    numbers[7] = enginereal->createTexture("./images/my_font/7.png");
    numbers[8] = enginereal->createTexture("./images/my_font/8.png");
    numbers[9] = enginereal->createTexture("./images/my_font/9.png");
    numbers[10] = enginereal->createTexture("./images/my_font/+.png");

    // loading ounds
    grottans::sound_buffer* sound_destroy_big_form = enginereal->createSoundBuffer("./sounds/02_destroy_big_form");
    grottans::sound_buffer* sound_level_yellow = enginereal->createSoundBuffer("./sounds/07_level_yellow");
    grottans::sound_buffer* sound_level_red = enginereal->createSoundBuffer("./sounds/04_level_red");
    grottans::sound_buffer* sound_game_over = enginereal->createSoundBuffer("./sounds/03_game_over");
    grottans::sound_buffer* sound_cant_flip = enginereal->createSoundBuffer("./sounds/09_cant_flip");
    grottans::sound_buffer* sound_on = enginereal->createSoundBuffer("./sounds/10_sound_on.wav");
    grottans::sound_buffer* sound_fall = enginereal->createSoundBuffer("./sounds/00_falling");
    grottans::sound_buffer* sound_flip = enginereal->createSoundBuffer("./sounds/08_flip");

    // loading vertex_buffers from files
    // tr0,1 for blocks, tr2,3 for counter bar, tr4,5 for count line
    // tr6,7 for button classic, tr8,9 for button extreme
    // tr 10,11 for 1 level number, tr12,13 for 2 level number
    std::ifstream file("vert_tex_color.txt");
    if (!file) {
        std::cerr << "can't load vert_tex_color.txt\n";
        return EXIT_FAILURE;
    } else {
        file >> tr[0] >> tr[1] >> tr[2] >> tr[3] >> tr[4] >> tr[5]
            >> tr[6] >> tr[7] >> tr[8] >> tr[9] >> tr[10] >> tr[11] >> tr[12] >> tr[13];
        if (!sizeof(tr[1])) {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }
    file.close();

    // loading tr_score for 1-5 numbers of score
    std::ifstream file_score("vert_tex_color_score.txt");
    if (!file_score) {
        std::cerr << "can't load vert_tex_color_score.txt\n";
        return EXIT_FAILURE;
    } else {
        file_score >> tr_score[0] >> tr_score[1] >> tr_score[2] >> tr_score[3]
            >> tr_score[4] >> tr_score[5] >> tr_score[6] >> tr_score[7] >> tr_score[8] >> tr_score[9];
        if (!sizeof(tr_score[1])) {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }
    file_score.close();

    // loading tr_score for 1-5 numbers of points
    std::ifstream file_points("vert_tex_points.txt");
    if (!file_points) {
        std::cerr << "can't load vert_tex_points.txt\n";
        return EXIT_FAILURE;
    } else {
        file_points >> tr_points[0] >> tr_points[1] >> tr_points[2] >> tr_points[3]
            >> tr_points[4] >> tr_points[5] >> tr_points[6] >> tr_points[7];
        if (!sizeof(tr_points[1])) {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }
    file_score.close();

    // loading tr_falling for v_buf (for falling animation)
    std::ifstream file_falling("vert_falling.txt");
    if (!file_falling) {
        std::cerr << "can't load vert_falling.txt\n";
        return EXIT_FAILURE;
    } else {
        file_falling >> tr_falling[0] >> tr_falling[1]; //>> tr_falling[2] >> tr_falling[3]
        //>> tr_falling[4] >> tr_falling[5] >> tr_falling[6] >> tr_falling[7];
        if (!sizeof(tr_falling[1])) {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }
    file_falling.close();

    /*
    // scaling the texture of the blocks according to the window size
    // scaling blocks vertex buffers
    for (size_t i = 0; i < 2; i++) {
        for (size_t j = 0; j < 3; j++) {
            //tr[i].v[j].pos.x *= WIDTH / 240.;
            //tr[i].v[j].pos.y *= HEIGHT / 268.;
        }
    }

    // scaling blocks vertex buffers
    for (size_t i = 0; i < tr_falling.size(); i++) {
        for (size_t j = 0; j < 3; j++) {
            //tr_falling[i].v[j].pos.x *= WIDTH / 240.;
            //tr_falling[i].v[j].pos.y *= HEIGHT / 268.;
        }
    }
    */

    //creating 16 v_buf's for blocks falling
    std::array<grottans::vertex_buffer*, 16> v_falling_buffers;
    for (size_t i = 0; i < 16; i++) {
        v_falling_buffers.at(i) = enginereal->createVertexBuffer(&tr_falling[0], 2); //low //full
        tr_falling[0].v[0].uv.y -= 0.0625f;
        tr_falling[0].v[1].uv.y -= 0.0625f;
        tr_falling[0].v[2].uv.y -= 0.0625f;
        tr_falling[1].v[0].uv.y -= 0.0625f;
        tr_falling[1].v[1].uv.y -= 0.0625f;
        tr_falling[1].v[2].uv.y -= 0.0625f;
    }

    // creating 0..99 new blocks
    for (size_t i = 0; i < 10; i++) {
        for (size_t j = 0; j < 10; j++) {
            size_t index = i * 10 + j;

            MAP[index] = new Block();
            // const
            MAP[index]->position = { -9.f + j * 2, 10.f - i * 2 };
            // maybe need to moving to the rendering part !!!!!!!!!!!
            //enginereal->createVertexBuffer(&tr[0], 2);
            MAP[index]->v_buf = v_falling_buffers[15];
        }
    }

    //MAP[99]->aspect = grottans::mat2x3::scale(BLOCK / WIDTH * 0.75f, BLOCK / HEIGTH * 0.75f);

    // creating selection block in center of the field
    MAP[100] = new Block();
    MAP[100]->position = { OFFSET / 2, 0.f };
    MAP[100]->v_buf = enginereal->createVertexBuffer(&tr[0], 2);
    MAP[100]->texture = tex_selected;

    // button classic coord on select_mode screen
    size_t butt_cl_x_left = static_cast<size_t>((1 + tr[6].v[0].pos.x) * WIDTH / 2.);
    size_t butt_cl_x_right = static_cast<size_t>((1 + tr[6].v[1].pos.x) * WIDTH / 2.);
    size_t butt_cl_y_up = static_cast<size_t>((1 - tr[6].v[0].pos.y) * HEIGHT / 2.);
    size_t butt_cl_y_down = static_cast<size_t>((1 - tr[6].v[2].pos.y) * HEIGHT / 2.);
    // button extreme coord on select_mode screen
    size_t butt_ext_x_left = static_cast<size_t>((1 + tr[8].v[0].pos.x) * WIDTH / 2.);
    size_t butt_ext_x_right = static_cast<size_t>((1 + tr[8].v[1].pos.x) * WIDTH / 2.);
    size_t butt_ext_y_up = static_cast<size_t>((1 - tr[8].v[0].pos.y) * HEIGHT / 2.);
    size_t butt_ext_y_down = static_cast<size_t>((1 - tr[8].v[2].pos.y) * HEIGHT / 2.);

    ///////////////////////////////////////////////////////////////////////////
    // universal elements for rendering
    grottans::vec2 null_move(0.f, 0.f);
    grottans::mat2x3 null_scale = grottans::mat2x3::scale(1.f, 1.f);
    grottans::mat2x3 MAP_scale = grottans::mat2x3::scale(BLOCK / static_cast<double>(WIDTH), BLOCK / static_cast<double>(HEIGHT));

    // numbers of score in game mode
    std::unique_ptr<Block> number_1(new Block);
    number_1->v_buf = enginereal->createVertexBuffer(&tr_score[0], 2);
    number_1->move = grottans::mat2x3::move(null_move);
    number_1->aspect = null_scale;
    grottans::mat2x3 m_number_1 = number_1->move * number_1->aspect;
    std::unique_ptr<Block> number_2(new Block);
    number_2->v_buf = enginereal->createVertexBuffer(&tr_score[2], 2);
    number_2->move = grottans::mat2x3::move(null_move);
    number_2->aspect = null_scale;
    grottans::mat2x3 m_number_2 = number_2->move * number_2->aspect;
    std::unique_ptr<Block> number_3(new Block);
    number_3->v_buf = enginereal->createVertexBuffer(&tr_score[4], 2);
    number_3->move = grottans::mat2x3::move(null_move);
    number_3->aspect = null_scale;
    grottans::mat2x3 m_number_3 = number_3->move * number_3->aspect;
    std::unique_ptr<Block> number_4(new Block);
    number_4->v_buf = enginereal->createVertexBuffer(&tr_score[6], 2);
    number_4->move = grottans::mat2x3::move(null_move);
    number_4->aspect = null_scale;
    grottans::mat2x3 m_number_4 = number_4->move * number_4->aspect;
    std::unique_ptr<Block> number_5(new Block);
    number_5->v_buf = enginereal->createVertexBuffer(&tr_score[8], 2);
    number_5->move = grottans::mat2x3::move(null_move);
    number_5->aspect = null_scale;
    grottans::mat2x3 m_number_5 = number_5->move * number_5->aspect;

    // numbers of points in game mode
    std::unique_ptr<Block> points_1(new Block);
    points_1->v_buf = enginereal->createVertexBuffer(&tr_points[0], 2);
    points_1->move = grottans::mat2x3::move(null_move);
    points_1->aspect = null_scale;
    grottans::mat2x3 m_points_1 = points_1->move * points_1->aspect;
    std::unique_ptr<Block> points_2(new Block);
    points_2->v_buf = enginereal->createVertexBuffer(&tr_points[2], 2);
    points_2->move = grottans::mat2x3::move(null_move);
    points_2->aspect = null_scale;
    grottans::mat2x3 m_points_2 = points_2->move * points_2->aspect;
    std::unique_ptr<Block> points_3(new Block);
    points_3->v_buf = enginereal->createVertexBuffer(&tr_points[4], 2);
    points_3->move = grottans::mat2x3::move(null_move);
    points_3->aspect = null_scale;
    grottans::mat2x3 m_points_3 = points_3->move * points_3->aspect;
    std::unique_ptr<Block> points_4(new Block);
    points_4->v_buf = enginereal->createVertexBuffer(&tr_points[6], 2);
    points_4->move = grottans::mat2x3::move(null_move);
    points_4->aspect = null_scale;
    grottans::mat2x3 m_points_4 = points_4->move * points_4->aspect;

    // level_complite (new level)
    std::unique_ptr<Block> block_level(new Block);
    block_level->v_buf = enginereal->createVertexBuffer(&tr[0], 2);
    block_level->move = grottans::mat2x3::move(null_move);
    block_level->aspect = grottans::mat2x3::scale(1.f, 1.f /*240. / WIDTH, 268. / HEIGHT*/);
    grottans::mat2x3 m_level = block_level->move * block_level->aspect;
    // left number of level
    std::unique_ptr<Block> level_number_one(new Block);
    level_number_one->v_buf = enginereal->createVertexBuffer(&tr[10], 2);
    level_number_one->move = grottans::mat2x3::move(null_move);
    level_number_one->aspect = null_scale;
    grottans::mat2x3 m_level_number_1 = level_number_one->move * level_number_one->aspect;
    // right number of level
    std::unique_ptr<Block> level_number_two(new Block);
    level_number_two->v_buf = enginereal->createVertexBuffer(&tr[12], 2);
    level_number_two->move = grottans::mat2x3::move(null_move);
    level_number_two->aspect = null_scale;
    grottans::mat2x3 m_level_number_2 = level_number_two->move * level_number_two->aspect;
    // game_over mode
    std::unique_ptr<Block> block_game_over(new Block);
    block_game_over->v_buf = enginereal->createVertexBuffer(&tr[0], 2);
    block_game_over->texture = tex_game_over;
    block_game_over->move = grottans::mat2x3::move(null_move);
    block_game_over->aspect = grottans::mat2x3::scale(1.f, 1.f /*240. / WIDTH, 268. / HEIGHT*/);
    grottans::mat2x3 m_game_over = block_game_over->move * block_game_over->aspect;
    // counter background
    std::unique_ptr<Block> block_counter(new Block);
    block_counter->v_buf = enginereal->createVertexBuffer(&tr[2], 2);
    block_counter->texture = tex_counter_empty;
    block_counter->move = grottans::mat2x3::move(null_move);
    block_counter->aspect = null_scale;
    grottans::mat2x3 m_counter = block_counter->move * block_counter->aspect;
    // counter line
    std::unique_ptr<Block> block_line(new Block);
    block_line->texture = tex_counter_full;
    block_line->move = grottans::mat2x3::move(null_move);
    block_line->aspect = null_scale;
    grottans::mat2x3 m_line = block_counter->move * block_counter->aspect;
    // game type selecting
    std::unique_ptr<Block> block_main(new Block);
    block_main->v_buf = enginereal->createVertexBuffer(&tr[0], 2);
    block_main->texture = tex_select_game_type;
    block_main->move = grottans::mat2x3::move(null_move);
    block_main->aspect = grottans::mat2x3::scale(1.f, 1.f /*240. / WIDTH, 268. / HEIGHT*/);
    grottans::mat2x3 m_type = block_main->move * block_main->aspect;
    // classic button
    std::unique_ptr<Block> block_classic(new Block);
    block_classic->v_buf = enginereal->createVertexBuffer(&tr[6], 2);
    block_classic->texture = tex_classic;
    block_classic->move = grottans::mat2x3::move(null_move);
    block_classic->aspect = null_scale;
    grottans::mat2x3 m_classic = block_classic->move * block_classic->aspect;
    // extreme button
    std::unique_ptr<Block> block_extreme(new Block);
    block_extreme->v_buf = enginereal->createVertexBuffer(&tr[8], 2);
    block_extreme->texture = tex_extreme;
    block_extreme->move = grottans::mat2x3::move(null_move);
    block_extreme->aspect = null_scale;
    grottans::mat2x3 m_extreme = block_extreme->move * block_extreme->aspect;

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    // MAIN LOOP

    clock_timer timer;
    time_point start = timer.now();

    grottans::event e;

    bool continue_loop = true;
    while (continue_loop) {
        time_point end_last_frame = timer.now();

        while (enginereal->input(e)) {
            if (e == grottans::event::turn_off) {
                continue_loop = false;
#ifdef _WIN32
                std::exit(EXIT_SUCCESS);
#endif
            }
            std::cout << e << std::endl;

            // ignoring mouse moving events
            if (e == grottans::event::mouse_motion) {
                continue;
            }

            // SPACE - back to the first screen OR GAME_COMLITE
            if (e == grottans::event::button2_released || level_number > 25) {
                MODE = mode::select_type_game;
                TYPE = type::classic;
                SCORE = 0;
                DELTA_SCORE = 0;
                delta_points = 0;
                level_number = 1;
                first_time_click = 0;
                score_in_the_end_of_level = 0;
                START = false;
                enginereal->set_window_title("WinMo Bubbles");
            }

            if (MODE == mode::select_type_game) {
                switch (e) {
                /*  case grottans::event::mouse_motion: {
                    if (enginereal->mouse_coord.x < WIDTH / 2) {
                        TYPE = type::classic;
                    } else
                        TYPE = type::extreme;
                    continue; // next iteration of main loop
                    //break;
                } */
                case grottans::event::mouse_pressed: {
                    //check button classic_mode
                    if (enginereal->mouse_coord.x >= butt_cl_x_left && enginereal->mouse_coord.x <= butt_cl_x_right) {
                        if (enginereal->mouse_coord.y <= butt_cl_y_down && enginereal->mouse_coord.y >= butt_cl_y_up) {
                            TYPE = type::classic;
                            new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                                tex_red, tex_black, tr, TYPE);
                            MODE = mode::game_classic;
                            START = true;
                            enginereal->set_window_title("classic mode");
                        }
                    }
                    //check button extreme_mode
                    if (enginereal->mouse_coord.x >= butt_ext_x_left && enginereal->mouse_coord.x <= butt_ext_x_right) {
                        if (enginereal->mouse_coord.y <= butt_ext_y_down && enginereal->mouse_coord.y >= butt_ext_y_up) {
                            TYPE = type::extreme;
                            new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                                tex_red, tex_black, tr, TYPE);
                            MODE = mode::game_extreme;
                            START = true;
                            enginereal->set_window_title("extreme mode");
                        }
                    }
                    // check SOUND turn-on/off
                    if (enginereal->mouse_coord.y < butt_ext_y_up && enginereal->mouse_coord.y > HEIGHT / 2) {
                        if (enginereal->mouse_coord.x > WIDTH / 4 && enginereal->mouse_coord.x < WIDTH * 3 / 4) {
                            sound = !sound;
                            if (sound) {
                                sound_on->play(grottans::sound_buffer::properties::once);
                            }
                        }
                    }
                    if (enginereal->mouse_coord.x < WIDTH / 2) {
                        TYPE = type::classic;
                    } else {
                        TYPE = type::extreme;
                    }
                    break;
                }
                case grottans::event::start_pressed: {
                    START = true;
                    if (TYPE == type::classic) {
                        new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                            tex_red, tex_black, tr, TYPE);
                        MODE = mode::game_classic;
                        enginereal->set_window_title("classic mode");
                    }
                    if (TYPE == type::extreme) {
                        new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                            tex_red, tex_black, tr, TYPE);
                        MODE = mode::game_extreme;
                        enginereal->set_window_title("extreme mode");
                    }
                    break;
                }
                case grottans::event::right_pressed: {
                    TYPE = type::extreme;
                    break;
                }
                case grottans::event::left_pressed: {
                    TYPE = type::classic;
                    break;
                }
                case grottans::event::up_pressed: {
                    sound = !sound;
                    if (sound) {
                        sound_on->play(grottans::sound_buffer::properties::once);
                    }
                    break;
                }
                }
                break;
            }

            if (START) {
                switch (MODE) {
                case mode::game_classic: {
                    if (first_time_click < 1) {
                        first_time_click++;
                        break;
                    }

                    if (is_game_over(MAP, TYPE)) {
                        MODE = mode::game_over;
                        if (sound) {
                            sound_game_over->play(grottans::sound_buffer::properties::once);
                        }
                    }

                    if (e == grottans::event::mouse_released) {
                        size_t j = floor(enginereal->mouse_coord.x / static_cast<double>(WIDTH) * 10);
                        size_t i = floor(enginereal->mouse_coord.y / static_cast<double>((HEIGHT - BLOCK)) * 10);
                        if (i > 9)
                            break;
                        MAP[100]->position = { -9.f + j * 2, 10.f - i * 2 };

                        e = grottans::event::start_pressed;
                    }

                    switch (e) {
                    case grottans::event::left_pressed: {
                        if (MAP[100]->position.x > -9.f) {
                            MAP[100]->move.delta.x -= OFFSET;
                            MAP[100]->position.x -= OFFSET;
                        } else {
                            MAP[100]->move.delta.x = 9.f;
                            MAP[100]->position.x = 9.f;
                        }
                        break;
                    }
                    case grottans::event::right_pressed: {
                        if (MAP[100]->position.x < 9.f) {
                            MAP[100]->move.delta.x += OFFSET;
                            MAP[100]->position.x += OFFSET;
                        } else {
                            MAP[100]->move.delta.x = -9.f;
                            MAP[100]->position.x = -9.f;
                        }
                        break;
                    }
                    case grottans::event::up_pressed: {
                        if (MAP[100]->position.y < 10.f) {
                            MAP[100]->move.delta.y += OFFSET;
                            MAP[100]->position.y += OFFSET;
                        } else {
                            MAP[100]->move.delta.y = -8.f;
                            MAP[100]->position.y = -8.f;
                        }
                        break;
                    }
                    case grottans::event::down_pressed: {
                        if (MAP[100]->position.y > -8.f) {
                            MAP[100]->move.delta.y -= OFFSET;
                            MAP[100]->position.y -= OFFSET;
                        } else {
                            MAP[100]->move.delta.y = 10.f;
                            MAP[100]->position.y = 10.f;
                        }
                        break;
                    }
                    case grottans::event::start_pressed: {
                        //case grottans::event::button2_pressed: {
                        // selecting block under cursor
                        // calculating coordinates of the block under the cursor
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2);
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2);

                        // set the flags, skip if cell is empty
                        bool search = can_select(MAP, i, j);

                        // if block can be selected - marking them
                        if (search && MAP[i * 10 + j]->visible == true && MAP[i * 10 + j]->color != palette::non) {
                            MAP[i * 10 + j]->selected = true;
                            //MAP[i * 10 + j]->visible = false;
                            std::cout << i << ' ' << j << '\n'
                                      << std::endl;

                            // searching all selected blocks
                            DELTA_SCORE = selecting(MAP);

                            // sound
                            if (DELTA_SCORE > 0) {
                                if (sound) {
                                    sound_fall->play(
                                        grottans::sound_buffer::properties::once);
                                }
                            }
                            // sound destroy_big_form
                            if (DELTA_SCORE > 9) {
                                if (sound) {
                                    sound_destroy_big_form->play(
                                        grottans::sound_buffer::properties::once);
                                }
                            }

                            // translating delta to points
                            delta_points = delta_to_points(DELTA_SCORE, points, points_extreme, TYPE);
                            // increasing score
                            SCORE += delta_points;

                            // moving counter line on % step 0,0143
                            tr[4].v[1].pos.x += (0.0143f * delta_points * 100 / (levels[level_number]));
                            tr[4].v[2].pos.x += (0.0143f * delta_points * 100 / (levels[level_number]));
                            tr[5].v[1].pos.x += (0.0143f * delta_points * 100 / (levels[level_number]));

                            // level complite
                            if ((SCORE - score_in_the_end_of_level) >= levels[level_number]) {
                                //saving score
                                score_in_the_end_of_level = SCORE;

                                // next level
                                level_number++;

                                //changing window title
                                std::string title = "classic level ";
                                std::string num = ToString(level_number);
                                const char* title_and_num = (title + num).c_str();
                                enginereal->set_window_title(title_and_num);

                                //enginereal->set_window_title(title[level_number - 1]);

                                MODE = mode::level_complite;

                                first_time_click = 0;

                                if (level_number % 2) {
                                    if (sound) {
                                        sound_level_yellow->play(
                                            grottans::sound_buffer::properties::once);
                                    }
                                } else {
                                    if (sound) {
                                        sound_level_red->play(
                                            grottans::sound_buffer::properties::once);
                                    }
                                }
                            }

                        } else { // if block unselectable
                            // sound_impossible->play(grottans::sound_buffer::properties::once);
                        }
                    }
                    }
                    break;
                }
                case mode::game_extreme: {
                    if (first_time_click < 1) {
                        first_time_click++;
                        break;
                    }

                    if (is_game_over(MAP, TYPE)) {
                        MODE = mode::game_over;
                        if (sound) {
                            sound_game_over->play(grottans::sound_buffer::properties::once);
                        }
                    }

                    if (e == grottans::event::mouse_pressed) {
                        size_t j = floor(enginereal->mouse_coord.x / static_cast<double>(WIDTH) * 10);
                        size_t i = floor(enginereal->mouse_coord.y / (HEIGHT - WIDTH / 10.f) * 10);
                        if (i > 9)
                            break;
                        mouse_x_pressed = enginereal->mouse_coord.x;
                        mouse_y_pressed = enginereal->mouse_coord.y;

                        MAP[100]->position = { -9.f + j * 2, 10.f - i * 2 };

                        e = grottans::event::start_pressed;
                    }

                    if (e == grottans::event::mouse_released) {
                        mouse_x_released = enginereal->mouse_coord.x;
                        mouse_y_released = enginereal->mouse_coord.y;

                        int delta_x = mouse_x_pressed - mouse_x_released;
                        int delta_y = mouse_y_pressed - mouse_y_released;

                        // do not handle small offsets
                        if (abs(delta_x) < WIDTH / 20.f && abs(delta_y) < (HEIGHT - WIDTH / 10.f) / 20.f) {
                            MAP[100]->texture = tex_selected;
                            break;
                        }
                        // first quadrant
                        if (delta_x >= 0 && delta_y >= 0) {
                            if (abs(delta_x) >= abs(delta_y)) {
                                e = grottans::event::left_pressed;
                            } else {
                                e = grottans::event::up_pressed;
                            }
                        }
                        // third
                        if (delta_x >= 0 && delta_y < 0) {
                            if (abs(delta_x) >= abs(delta_y)) {
                                e = grottans::event::left_pressed;
                            } else {
                                e = grottans::event::down_pressed;
                            }
                        }
                        // second quardant
                        if (delta_x < 0 && delta_y >= 0) {
                            if (abs(delta_x) >= abs(delta_y)) {
                                e = grottans::event::right_pressed;
                            } else {
                                e = grottans::event::up_pressed;
                            }
                        }
                        // fourth quadrant
                        if (delta_x < 0 && delta_y < 0) {
                            if (abs(delta_x) >= abs(delta_y)) {
                                e = grottans::event::right_pressed;
                            } else {
                                e = grottans::event::down_pressed;
                            }
                        }

                    } // end of mouse_released

                    switch (e) {
                    case grottans::event::left_pressed: {
                        MAP[100]->motion = false;
                        // calculating coordinates selection block
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2); // x
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2); // y

                        if (MAP[i * 10 + j]->motion == false) {
                            if (MAP[100]->position.x > -9.f) {
                                MAP[100]->move.delta.x -= OFFSET;
                                MAP[100]->position.x -= OFFSET;
                            } else {
                                MAP[100]->move.delta.x = 9.f;
                                MAP[100]->position.x = 9.f;
                            }
                        } else {
                            if (can_flip(MAP, i, j, direction::left)) {
                                // fliping on 180 with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::left;
                                half = 3;

                                if (MAP[100]->position.x > -9.f) {
                                    MAP[100]->move.delta.x -= OFFSET;
                                    MAP[100]->position.x -= OFFSET;
                                } else {
                                    MAP[100]->move.delta.x = 9.f;
                                    MAP[100]->position.x = 9.f;
                                }
                                if (sound) {
                                    sound_flip->play(grottans::sound_buffer::properties::once);
                                }
                            } else {
                                // fliping on 360 with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::left;
                                half = 7;
                                if (sound) {
                                    sound_cant_flip->play(grottans::sound_buffer::properties::once);
                                }
                            }
                            MAP[i * 10 + j]->motion = false;
                        }

                        break;
                    }
                    case grottans::event::right_pressed: {
                        MAP[100]->motion = false;
                        // calculating coordinates selection block
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2); // x
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2); // y

                        if (MAP[i * 10 + j]->motion == false) {
                            if (MAP[100]->position.x < 9.f) {
                                MAP[100]->move.delta.x += OFFSET;
                                MAP[100]->position.x += OFFSET;
                            } else {
                                MAP[100]->move.delta.x = -9.f;
                                MAP[100]->position.x = -9.f;
                            }
                        } else {
                            if (can_flip(MAP, i, j, direction::right)) {
                                // fliping with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::right;
                                half = 3;
                                //replace_blocks(MAP, i, j, i, j + 1);
                                if (MAP[100]->position.x < 9.f) {
                                    MAP[100]->move.delta.x += OFFSET;
                                    MAP[100]->position.x += OFFSET;
                                } else {
                                    MAP[100]->move.delta.x = -9.f;
                                    MAP[100]->position.x = -9.f;
                                }
                                if (sound) {
                                    sound_flip->play(grottans::sound_buffer::properties::once);
                                }
                            } else {
                                // fliping on 360 with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::right;
                                half = 7;
                                if (sound) {
                                    sound_cant_flip->play(grottans::sound_buffer::properties::once);
                                }
                            }

                            MAP[i * 10 + j]->motion = false;
                        }
                        break;
                    }
                    case grottans::event::up_pressed: {
                        MAP[100]->motion = false;
                        // calculating coordinates selection block
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2); // x
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2); // y

                        if (MAP[i * 10 + j]->motion == false) {
                            if (MAP[100]->position.y < 10.f) {
                                MAP[100]->move.delta.y += OFFSET;
                                MAP[100]->position.y += OFFSET;
                            } else {
                                MAP[100]->move.delta.y = -8.f;
                                MAP[100]->position.y = -8.f;
                            }
                        } else {
                            if (can_flip(MAP, i, j, direction::up)) {
                                // fliping with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::up;
                                half = 3;
                                //replace_blocks(MAP, i, j, i - 1, j);
                                if (MAP[100]->position.y < 10.f) {
                                    MAP[100]->move.delta.y += OFFSET;
                                    MAP[100]->position.y += OFFSET;
                                } else {
                                    MAP[100]->move.delta.y = -8.f;
                                    MAP[100]->position.y = -8.f;
                                }
                                if (sound) {
                                    sound_flip->play(grottans::sound_buffer::properties::once);
                                }
                            } else {
                                // fliping on 360 with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::up;
                                half = 7;
                                if (sound) {
                                    sound_cant_flip->play(grottans::sound_buffer::properties::once);
                                }
                            }

                            MAP[i * 10 + j]->motion = false;
                        }
                        break;
                    }
                    case grottans::event::down_pressed: {
                        MAP[100]->motion = false;
                        // calculating coordinates selection block
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2); // x
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2); // y

                        if (MAP[i * 10 + j]->motion == false) {
                            if (MAP[100]->position.y > -8.f) {
                                MAP[100]->move.delta.y -= OFFSET;
                                MAP[100]->position.y -= OFFSET;
                            } else {
                                MAP[100]->move.delta.y = 10.f;
                                MAP[100]->position.y = 10.f;
                            }
                        } else {
                            if (can_flip(MAP, i, j, direction::down)) {
                                // fliping with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::down;
                                half = 3;
                                //replace_blocks(MAP, i, j, i + 1, j);
                                if (MAP[100]->position.y > -8.f) {
                                    MAP[100]->move.delta.y -= OFFSET;
                                    MAP[100]->position.y -= OFFSET;
                                } else {
                                    MAP[100]->move.delta.y = 10.f;
                                    MAP[100]->position.y = 10.f;
                                }
                                if (sound) {
                                    sound_flip->play(grottans::sound_buffer::properties::once);
                                }
                            } else {
                                // fliping on 360 with animation
                                MODE = mode::flip;
                                m = i;
                                n = j;
                                flip = direction::down;
                                half = 7;
                                if (sound) {
                                    sound_cant_flip->play(grottans::sound_buffer::properties::once);
                                }
                            }

                            MAP[i * 10 + j]->motion = false;
                        }
                        break;
                    }
                    case grottans::event::start_pressed: {
                        // selecting block under cursor
                        // calculating coordinates of the block under the cursor
                        size_t j = static_cast<size_t>((MAP[100]->position.x + 9) / 2); // x
                        size_t i = static_cast<size_t>((10 - MAP[100]->position.y) / 2); // y

                        if (MAP[i * 10 + j]->color == palette::black)
                            continue;

                        if (MAP[i * 10 + j]->motion == false) {
                            //clean flags previous blocks
                            motion_reset(MAP);
                            //mark before fliping
                            MAP[i * 10 + j]->motion = true;
                            // for changing texture
                            MAP[100]->motion = true;
                        } else {
                            //selecting blocks and deleting
                            MAP[i * 10 + j]->selected = true;

                            if (MAP[i * 10 + j]->color == palette::bomb) {
                                select_around(MAP, i, j);
                                DELTA_SCORE = 9;
                            } else {
                                // searching all selected blocks
                                DELTA_SCORE = selecting(MAP);
                            }

                            // check for the 3+ blocks
                            if (DELTA_SCORE >= 3) {

                                if (sound) {
                                    sound_fall->play(
                                        grottans::sound_buffer::properties::once);
                                }

                                // sound destroy_big_form
                                if (DELTA_SCORE > 15 && sound) {
                                    sound_destroy_big_form->play(
                                        grottans::sound_buffer::properties::once);
                                }

                                // translating delta to points
                                delta_points = delta_to_points(DELTA_SCORE, points, points_extreme, TYPE);
                                // increasing score
                                SCORE += delta_points;

                                // moving counter line on % step 0,0143
                                tr[4].v[1].pos.x += (0.0143f * delta_points * 100 / (levels_extreme[level_number]));
                                tr[4].v[2].pos.x += (0.0143f * delta_points * 100 / (levels_extreme[level_number]));
                                tr[5].v[1].pos.x += (0.0143f * delta_points * 100 / (levels_extreme[level_number]));

                                // level complite
                                if ((SCORE - score_in_the_end_of_level) >= levels_extreme[level_number]) {
                                    //saving score
                                    score_in_the_end_of_level = SCORE;

                                    // next level
                                    level_number++;

                                    //changing window title
                                    std::string title = "extreme level ";
                                    std::string num = ToString(level_number);
                                    const char* title_and_num = (title + num).c_str();
                                    enginereal->set_window_title(title_and_num);

                                    MODE = mode::level_complite;

                                    first_time_click = 0;

                                    if (level_number % 2) {
                                        if (sound) {
                                            sound_level_yellow->play(
                                                grottans::sound_buffer::properties::once);
                                        }
                                    } else {
                                        if (sound) {
                                            sound_level_red->play(
                                                grottans::sound_buffer::properties::once);
                                        }
                                    }
                                }

                                motion_reset(MAP);
                            } else {
                                //if blocks <3 then unselecting all f
                                unselect_all(MAP);
                            }
                        }
                        break;
                    }
                    }
                    break;
                }
                case mode::game_over: {
                    if (e == grottans::event::start_pressed || e == grottans::event::mouse_pressed) {
                        MODE = mode::select_type_game;
                        TYPE = type::classic;
                        SCORE = 0;
                        DELTA_SCORE = 0;
                        delta_points = 0;
                        level_number = 1;
                        first_time_click = 0;
                        score_in_the_end_of_level = 0;
                        START = false;
                        enginereal->set_window_title("WinMo Bubbles");
                        break;
                    }
                    break;
                }
                case mode::level_complite: {
                    if (e == grottans::event::start_pressed || e == grottans::event::mouse_pressed)
                        MODE = mode::new_level;
                    break;
                }
                case mode::new_level: {
                    //if (level_number >25) MODE = mode::select_type_game;

                    if (TYPE == type::classic) {
                        new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                            tex_red, tex_black, tr, TYPE);
                        MODE = mode::game_classic;
                    }
                    if (TYPE == type::extreme) {
                        new_level(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
                            tex_red, tex_black, tr, TYPE);
                        MODE = mode::game_extreme;
                    }
                    break;
                }
                }
            }
        }

        //  if (DELTA_SCORE) {
        //      popup_points(DELTA_SCORE, MAP[100]->position.x, MAP[100]->position.y);
        //  }

        ///////////////////////////////////////////////////////////////////////
        // falling, shifting blocks and adding columns

        replace_blocks_with_animation(half, MAP, v_falling_buffers, m, n, flip);

        drop_animation(MAP, v_falling_buffers);

        add_columns_and_shift(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
            tex_red);

        add_top_blocks(MAP, tex_yellow, tex_purple, tex_green, tex_blue,
            tex_red, tex_black, tex_bomb);

        //std::cout << "mode: " << static_cast<int>(MODE) << std::endl;

        ///////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////
        // RENDERING:
        if (MODE == mode::select_type_game) {
            // background
            if (sound) {
                block_main->texture = tex_select_game_type;
            } else {
                block_main->texture = tex_select_game_type_no_sound;
            }

            enginereal->render(*block_main->v_buf, block_main->texture, m_type);
            if (TYPE == type::classic) {
                enginereal->render(*block_classic->v_buf, block_classic->texture,
                    m_classic);
            }
            if (TYPE == type::extreme) {
                enginereal->render(*block_extreme->v_buf, block_extreme->texture,
                    m_extreme);
            }
        }

        /*
        if (MODE == mode::game_classic || MODE == mode::new_level || MODE == mode::game_extreme ||
        MODE == mode::falling || MODE == mode::shifting || MODE == mode::adding || MODE == mode::flip) { */
        if (MODE != mode::select_type_game && MODE != mode::level_complite && MODE != mode::game_over) {
            // drawing a visible blocks, and unselecting them

            if (MODE == mode::flip && (flip == direction::right || flip == direction::down)) {
                // in flip mode with right or down direction
                for (int i = 100; i >= 0; i--) {
                    if (MODE != mode::falling && MODE != mode::shifting && MODE != mode::adding) {
                        MAP[i]->selected = false; // unselect all
                    }
                    //do not display selection block in falling mode
                    if ((MODE == mode::falling || MODE == mode::flip) && i == 100)
                        continue;

                    if (i == 100) {
                        if (MAP[100]->motion) {
                            MAP[100]->texture = tex_selected_move;
                        } else {
                            MAP[100]->texture = tex_selected;
                        }
                    }
                    if (MAP[i]->visible) {
                        MAP[i]->move.delta.x = /*WIDTH / 240. **/ MAP[i]->position.x;
                        MAP[i]->move.delta.y = /*HEIGHT / 268. **/ MAP[i]->position.y;
                        grottans::mat2x3 m_MAP = MAP[i]->move * MAP[i]->aspect * MAP_scale;
                        enginereal->render(*MAP[i]->v_buf, MAP[i]->texture, m_MAP);
                    }
                }
            } else { // all modes
                for (size_t i = 0; i < NUM_OF_BLOCKS; i++) {

                    if (MODE != mode::falling && MODE != mode::shifting && MODE != mode::adding) {
                        MAP[i]->selected = false; // unselect all
                    }
                    //do not display selection block in falling mode
                    if ((MODE == mode::falling || MODE == mode::flip) && i == 100)
                        continue;

                    if (i == 100) {
                        if (MAP[100]->motion) {
                            MAP[100]->texture = tex_selected_move;
                        } else {
                            MAP[100]->texture = tex_selected;
                        }
                    }

                    if (MAP[i]->visible) {
                        MAP[i]->move.delta.x = /*WIDTH / 240. **/ MAP[i]->position.x;
                        MAP[i]->move.delta.y = /*HEIGHT / 268. **/ MAP[i]->position.y;
                        grottans::mat2x3 m_MAP = MAP[i]->move * MAP[i]->aspect * MAP_scale;
                        enginereal->render(*MAP[i]->v_buf, MAP[i]->texture, m_MAP);
                    }
                }
            }

            // drawing counter background
            enginereal->render(*block_counter->v_buf, block_counter->texture,
                m_counter);
            // creating counter line with update tr coordinate
            block_line->v_buf = enginereal->createVertexBuffer(&tr[4], 2);
            enginereal->render(*block_line->v_buf, block_line->texture, m_line);

            // drawing score on the screen
            size_t score = SCORE;
            for (int i = 4; i >= 0; i--) {
                numbers_score[i] = score % 10;
                score = score / 10;
            }
            if (SCORE > 9999) {
                number_1->texture = numbers[numbers_score[0]]; //right number
                enginereal->render(*number_1->v_buf, number_1->texture, m_number_1);
            }
            if (SCORE > 999) {
                number_2->texture = numbers[numbers_score[1]];
                enginereal->render(*number_2->v_buf, number_2->texture, m_number_2);
            }
            if (SCORE > 99) {
                number_3->texture = numbers[numbers_score[2]];
                enginereal->render(*number_3->v_buf, number_3->texture, m_number_3);
            }
            if (SCORE > 9) {
                number_4->texture = numbers[numbers_score[3]];
                enginereal->render(*number_4->v_buf, number_4->texture, m_number_4);
            }
            number_5->texture = numbers[numbers_score[4]]; //left number
            enginereal->render(*number_5->v_buf, number_5->texture, m_number_5);

            //drawing points
            if (falling_counter) {
                size_t delta_points_tmp = delta_points;
                for (int i = 3; i >= 0; i--) {
                    numbers_points[i] = delta_points_tmp % 10;
                    delta_points_tmp = delta_points_tmp / 10;
                }

                points_1->texture = numbers[10]; //+
                enginereal->render(*points_1->v_buf, points_1->texture, m_points_1);
                if (delta_points > 99) {
                    points_2->texture = numbers[numbers_points[1]];
                    enginereal->render(*points_2->v_buf, points_2->texture, m_points_2);
                }
                if (delta_points > 9) {
                    points_3->texture = numbers[numbers_points[2]];
                    enginereal->render(*points_3->v_buf, points_3->texture, m_points_3);
                }
                points_4->texture = numbers[numbers_points[3]];
                enginereal->render(*points_4->v_buf, points_4->texture, m_points_4);
            }
        }

        if (MODE == mode::level_complite) {
            for (Block* x : MAP) { //turn-off falling in this mode
                x->selected = false; // unselect all block
            }

            // background
            if (level_number % 2) {
                block_level->texture = tex_level_yellow;
            } else {
                block_level->texture = tex_level_red;
            }

            enginereal->render(*block_level->v_buf, block_level->texture, m_level);

            int index_first_level_number = 0;
            int index_second_level_number = 0;
            if (level_number > 9) {
                index_first_level_number = level_number / 10;
                index_second_level_number = level_number % 10;
            } else {
                index_first_level_number = 0;
                index_second_level_number = static_cast<int>(level_number);
            }
            // left number
            if (level_number > 9) {
                level_number_one->texture = numbers[index_first_level_number];
                enginereal->render(*level_number_one->v_buf, level_number_one->texture,
                    m_level_number_1);
            }
            // right number
            level_number_two->texture = numbers[index_second_level_number];
            enginereal->render(*level_number_two->v_buf, level_number_two->texture,
                m_level_number_2);
        }

        if (MODE == mode::game_over) {
            SCORE = 0;
            DELTA_SCORE = 0;
            delta_points = 0;
            level_number = 1;
            first_time_click = 0;
            score_in_the_end_of_level = 0;
            for (size_t i = 0; i < NUM_OF_BLOCKS; i++) {
                MAP[i]->selected = false; // unselect all
                if (MAP[i]->visible) {
                    MAP[i]->move.delta.x = /*WIDTH / 240. **/ MAP[i]->position.x;
                    MAP[i]->move.delta.y = /*HEIGHT / 268. **/ MAP[i]->position.y;
                    grottans::mat2x3 m = MAP[i]->move * MAP[i]->aspect * MAP_scale;
                    enginereal->render(*MAP[i]->v_buf, MAP[i]->texture, m);
                }
            }
            enginereal->render(*block_game_over->v_buf, block_game_over->texture,
                m_game_over);
        }

        enginereal->swapBuffers();

        ///////////////////////////////////////////////////////////////////////
        // loop TIMER
        // after main loop
        milli_sec frame_delta = std::chrono::duration_cast<milli_sec>(end_last_frame - start);
        //std::cout << "time loop " << frame_delta.count() << std::endl;

        if (frame_delta.count() < 15) // 1000 % 60 = 16.666 FPS
        {
#ifdef _WIN32
            std::this_thread::sleep_for(std::chrono::duration<double, std::milli>(15));
#elif __linux__
            std::this_thread::yield(); // too fast, give other apps CPU time
#endif
            continue; // wait till more time
        }
        // in the end of the loop
        start = end_last_frame;

    } // end of main loop

    // cleaning memory before closing
    for (size_t i = 0; i < NUM_OF_BLOCKS; i++) {
        delete MAP[i];
    }

    enginereal->uninitialize();

    return EXIT_SUCCESS;
}
