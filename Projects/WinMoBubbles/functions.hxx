#pragma once

#include <array>
#include <sstream>

#include "engine.hxx"

//#define WIDTH 480.f // 640.f // 480.f //original 240.f
//#define HEIGTH 536.f // 715.f //536.f //original 268.f
extern size_t WIDTH;
extern size_t HEIGHT;
extern size_t BLOCK;

//#define BLOCK 24.f // original 24.f
#define NUM_OF_BLOCKS 101

extern float OFFSET;
extern size_t flip_counter;
extern size_t adding_counter;
extern size_t falling_counter;
extern size_t shifthing_counter;

enum class palette {
    yellow,
    purple,
    black,
    green,
    stone,
    blue,
    bomb,
    red,
    non
};

enum class type {
    classic,
    extreme,
    non
};

enum class mode {
    select_type_game,
    level_complite,
    game_classic,
    game_extreme,
    new_level,
    game_over,
    shifting,
    falling,
    adding,
    flip
};

enum class direction {
    right,
    down,
    left,
    non,
    up
};

extern type TYPE;
extern mode MODE;
extern mode MODE_copy;

struct Block {
    grottans::vec2 position{};
    palette color = palette::non;
    bool selected = false;
    bool visible = true;
    bool motion = false;
    grottans::mat2x3 move{};
    //grottans::mat2x3 aspect = grottans::mat2x3::scale(::BLOCK / static_cast<double>(::WIDTH), ::BLOCK / static_cast<double>(::HEIGHT));
    grottans::mat2x3 aspect = grottans::mat2x3::scale(1.f, 1.f);
    grottans::texture* texture = nullptr;
    grottans::vertex_buffer* v_buf = nullptr;
};

///////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
///
bool select_around(std::array<Block*, NUM_OF_BLOCKS>& MAP, const size_t& i, const size_t& j);

bool can_select(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const size_t& i, const size_t& j);

size_t selecting(std::array<Block*, NUM_OF_BLOCKS>& MAP);

void unselect_all(std::array<Block*, NUM_OF_BLOCKS>& MAP_);

void motion_reset(std::array<Block*, NUM_OF_BLOCKS>& MAP_);

void replace_blocks(std::array<Block*, NUM_OF_BLOCKS>& MAP_,
    const size_t& i, const size_t& j, const size_t& m, const size_t& n);

void replace_blocks_with_animation(size_t half, std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const std::array<grottans::vertex_buffer*, 16>& v_falling_buffers,
    const size_t& m, const size_t& n, direction dir);

bool can_flip(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const size_t& i, const size_t& j, direction dir);

bool is_game_over(std::array<Block*, NUM_OF_BLOCKS>& MAP, const type type_);

void new_level(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture* const& tex_yellow,
    grottans::texture* const& tex_purple,
    grottans::texture* const& tex_green,
    grottans::texture* const& tex_blue,
    grottans::texture* const& tex_red,
    grottans::texture* const& tex_black,
    std::array<grottans::tri2, 14>& tr,
    const type& TYPE);

bool drop_animation(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    const std::array<grottans::vertex_buffer*, 16>& v_falling_buffers);

bool add_columns_and_shift(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture*& tex_yellow,
    grottans::texture*& tex_purple,
    grottans::texture*& tex_green,
    grottans::texture*& tex_blue,
    grottans::texture*& tex_red);

bool add_top_blocks(std::array<Block*, NUM_OF_BLOCKS>& MAP,
    grottans::texture*& tex_yellow,
    grottans::texture*& tex_purple,
    grottans::texture*& tex_green,
    grottans::texture*& tex_blue,
    grottans::texture*& tex_red,
    grottans::texture*& tex_black,
    grottans::texture*& tex_bomb);

size_t delta_to_points(size_t delta, std::array<size_t, 25> points,
    std::array<size_t, 30> points_extreme, type t);

void popup_points(grottans::Engine*& eng, const size_t delta, const float x, const float y);

template <typename T>
std::string ToString(T t)
{
    std::stringstream ss;
    ss << t;
    return ss.str();
}
