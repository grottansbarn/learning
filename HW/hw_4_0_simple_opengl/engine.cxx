#include "engine.hxx"
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengles2.h>
#include "engine_real.hxx"

#include <assert.h>
#include <algorithm>
#include <array>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>

#define OM_GL_CHECK()                                                 \
  {                                                                   \
    const int err = glGetError();                                     \
    if (err != GL_NO_ERROR) {                                         \
      switch (err) {                                                  \
        case GL_INVALID_ENUM:                                         \
          std::cerr << GL_INVALID_ENUM << std::endl;                  \
          break;                                                      \
        case GL_INVALID_VALUE:                                        \
          std::cerr << GL_INVALID_VALUE << std::endl;                 \
          break;                                                      \
        case GL_INVALID_OPERATION:                                    \
          std::cerr << GL_INVALID_OPERATION << std::endl;             \
          break;                                                      \
        case GL_INVALID_FRAMEBUFFER_OPERATION:                        \
          std::cerr << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl; \
          break;                                                      \
        case GL_OUT_OF_MEMORY:                                        \
          std::cerr << GL_OUT_OF_MEMORY << std::endl;                 \
          break;                                                      \
      }                                                               \
      assert(false);                                                  \
    }                                                                 \
  }

namespace grottans {

static std::array<std::string_view, 17> event_names = {
    {/// input events
     "left_pressed", "left_released", "right_pressed", "right_released",
     "up_pressed", "up_released", "down_pressed", "down_released",
     "select_pressed", "select_released", "start_pressed", "start_released",
     "button1_pressed", "button1_released", "button2_pressed",
     "button2_released",
     /// virtual console events
     "turn_off"}};

struct bind {
  bind(SDL_Keycode k, std::string_view s, event pressed, event released)
      : key(k), name(s), event_pressed(pressed), event_released(released) {}

  SDL_Keycode key;
  std::string_view name;
  event event_pressed;
  event event_released;
};

const std::array<bind, 8> keys{
    {{SDLK_w, "up", event::up_pressed, event::up_released},
     {SDLK_a, "left", event::left_pressed, event::left_released},
     {SDLK_s, "down", event::down_pressed, event::down_released},
     {SDLK_d, "right", event::right_pressed, event::right_released},
     {SDLK_LCTRL, "button1", event::button1_pressed, event::button1_released},
     {SDLK_SPACE, "button2", event::button2_pressed, event::button2_released},
     {SDLK_ESCAPE, "select", event::select_pressed, event::select_released},
     {SDLK_RETURN, "start", event::start_pressed, event::start_released}}};

std::ostream& operator<<(std::ostream& stream, const event e) {
  std::uint32_t value = static_cast<std::uint32_t>(e);
  std::uint32_t minimal = static_cast<std::uint32_t>(event::left_pressed);
  std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
  if (value >= minimal && value <= maximal) {
    stream << event_names[value];
    return stream;
  } else {
    throw std::runtime_error("too big event value");
  }
}

std::istream& operator>>(std::istream& is, vertex& v) {
  is >> v.x;
  is >> v.y;
  return is;
}

std::istream& operator>>(std::istream& is, triangle& t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}

///////////////////////////////////////////////////////////////////////////////
bool check_input(const SDL_Event& e, const bind*& result) {
  using namespace std;

  const auto it = find_if(begin(keys), end(keys), [&](const bind& b) {
    return b.key == e.key.keysym.sym;
  });

  if (it != end(keys)) {
    result = &(*it);
    return true;
  }
  return false;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::initialize
/// \return
///
std::string EngineReal::initialize() {
  using namespace std;

  const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (init_result != 0) {
    const char* err_message = SDL_GetError();
    std::cerr << "error: initialization failed: " << err_message << std::endl;
  }

  /*SDL_Window* const */
  window = SDL_CreateWindow("First window on SDL with OpenGL and Keys",
                            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640,
                            480, ::SDL_WINDOW_OPENGL);
  if (window == nullptr) {
    const char* err_message = SDL_GetError();
    std::cerr << "error: Create Window failed: " << err_message << std::endl;
    SDL_Quit();
  }

  int gl_major_ver = 2;
  int gl_minor_ver = 0;

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  SDL_assert(gl_context != nullptr);

  int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
  SDL_assert(result == 0);

  result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
  SDL_assert(result == 0);

  if (gl_major_ver < 2) {
    std::clog << "current context opengl version: " << gl_major_ver << '.'
              << gl_minor_ver << '\n'
              << "need openg version at least: 2.1\n"
              << std::flush;
    throw std::runtime_error("opengl version too low");
  }

  return "";
}

/// pool event from input queue
/// return true if more events in queue
bool EngineReal::input(event& e) {
  using namespace std;
  while (true) {
    // collect all events from SDL
    SDL_Event sdl_event;
    if (SDL_WaitEvent(&sdl_event)) {  //ждем ивента сдл
      const bind* binding = nullptr;

      if (sdl_event.type == SDL_QUIT) {
        e = grottans::event::turn_off;
        return true;
      } else if (sdl_event.type == SDL_KEYDOWN) {
        if (check_input(sdl_event, binding)) {
          e = binding->event_pressed;  //????????????????????????????
          return true;
        }
      } else if (sdl_event.type == SDL_KEYUP) {
        if (check_input(sdl_event, binding)) {
          e = binding->event_released;  //????????????????????????????
          return true;
        }
      }
    }
    return false;
  }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::start
///
void EngineReal::start() {
  bool continue_loop = true;
  while (continue_loop) {
    event e;
    while (input(e)) {
      std::cout << e << std::endl;
      switch (e) {
        case grottans::event::turn_off:
          continue_loop = false;
          break;
        default:
          break;
      }
    }
    std::ifstream file("vertexes.txt");
    assert(!!file);

    grottans::triangle tr;
    file >> tr;

    renderTriangle(tr);

    swapBuffers();
  }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::renderTriangle
///
void EngineReal::renderTriangle(const triangle&) {
  glClearColor(0.f, 1.0, 0.75f, 0.0f);
  OM_GL_CHECK();
  glClear(GL_COLOR_BUFFER_BIT);
  OM_GL_CHECK();
}
///////////////////////////////////////////////////////////////////////////////
/// \brief swap_buffers
///
void EngineReal::swapBuffers() { SDL_GL_SwapWindow(window); }

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::uninitialize
///
void EngineReal::uninitialize() {
  SDL_DestroyWindow(window);
  SDL_Quit();
  destroy_engine(this);
}

// EngineReal::~EngineReal() {}

///////////////////////////////////////////////////////////////////////////////
static bool already_exist = false;

EngineReal* create_engine() {
  if (already_exist) {
    throw std::runtime_error("engine already exist");
  }
  EngineReal* result = new EngineReal();
  already_exist = true;
  return result;
}

void destroy_engine(EngineReal* e) {
  if (already_exist == false) {
    throw std::runtime_error("engine not created");
  }
  if (nullptr == e) {
    throw std::runtime_error("e is nullptr");
  }
  delete e;
}

}  // end of namespace grottans
