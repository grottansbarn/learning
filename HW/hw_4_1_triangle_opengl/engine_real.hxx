#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "engine.hxx"

namespace grottans {

class EngineReal final : public Engine {
  SDL_Window* window = nullptr;
  SDL_GLContext gl_context = nullptr;
  GLuint program_id_ = 0;

 public:
  std::string initialize();
  void start();
  bool input(event& e);
  void renderTriangle(const triangle&);
  void swapBuffers();
  void uninitialize();
  ~EngineReal() {}
};

EngineReal* create_engine();
void destroy_engine(EngineReal* e);
}  // end of namespace grottans
