#include "engine.hxx"
#include "engine_real.hxx"
#include <memory>

int main()
{
    using namespace std;

    grottans::EngineReal* enginereal = grottans::create_engine();

    enginereal->initialize();

    enginereal->start();

    enginereal->uninitialize();

    return EXIT_SUCCESS;
}
