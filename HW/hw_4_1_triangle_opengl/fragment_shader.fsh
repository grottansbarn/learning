precision mediump float;
varying vec4 v_position;
void main() {
if (v_position.z >= 0.0) {
    float light_red = 0.5 + v_position.z/2.0;
    gl_FragColor = vec4(light_red, 0.0, 0.0, 1.0);
} else {
    float dark_red = 0.5 - (v_position.z/-2.0);
    gl_FragColor = vec4(dark_red, 0.0, 0.0, 1.0);
}
}
