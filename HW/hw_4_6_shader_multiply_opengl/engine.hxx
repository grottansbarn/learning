#pragma once

#include <string>

namespace grottans {

struct vec2 {
    vec2();
    vec2(float x, float y);
    float x = 0;
    float y = 0;
};

vec2 operator+(const vec2& l, const vec2& r);

struct mat2 {
    mat2();
    static mat2 identiry();
    static mat2 scale(float scale);
    static mat2 rotation(float thetha);
    vec2 col0;
    vec2 col1;
};

struct mat2x3 {
    mat2x3();
    static mat2x3 identiry();
    static mat2x3 scale(float scale);
    static mat2x3 rotation(float thetha);
    static mat2x3 move(const vec2& delta);
    vec2 col0;
    vec2 col1;
    vec2 delta;
};

vec2 operator*(const vec2& v, const mat2& m);
mat2 operator*(const mat2& m1, const mat2& m2);
vec2 operator*(const vec2& v, const mat2x3& m);
mat2x3 operator*(const mat2x3& m1, const mat2x3& m2);

enum class event {
    // input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button1_pressed,
    button1_released,
    button2_pressed,
    button2_released,
    turn_off
};

class color {
public:
    color() = default;
    explicit color(std::uint32_t rgba_);
    color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

/// position in 2d space
struct pos {
    float x = 0.f;
    float y = 0.f;
};

/// texture position (normalized)
struct uv_pos {
    float u = 0.f;
    float v = 0.f;
};

/// vertex with position only
struct v0 {
    vec2 pos;
};

/// vertex with position and texture coordinate
struct v1 {
    vec2 pos;
    color c;
};
/// vertex position + color + texture coordinate
struct v2 {
    vec2 pos;
    vec2 uv;
    color c;
};

/// triangle with positions only
struct tri0 {
    tri0();
    v0 v[3];
};

/// triangle with positions and color
struct tri1 {
    tri1();
    v1 v[3];
};

/// triangle with positions color and texture coordinate
struct tri2 {
    tri2();
    v2 v[3];
};

struct vertex {
    vertex()
        : x(0.f)
        , y(0.f)
        , tx(0.f)
        , ty(0.f)
    {
    }
    float x;
    float y;
    float tx;
    float ty;
};

struct triangle {
    triangle()
    {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

std::ostream& operator<<(std::ostream& stream, const event e);
std::istream& operator>>(std::istream& is, vertex&);
std::istream& operator>>(std::istream& is, triangle&);

std::istream& operator>>(std::istream& is, uv_pos&);
std::istream& operator>>(std::istream& is, color&);
std::istream& operator>>(std::istream& is, v0&);
std::istream& operator>>(std::istream& is, v1&);
std::istream& operator>>(std::istream& is, v2&);
std::istream& operator>>(std::istream& is, tri0&);
std::istream& operator>>(std::istream& is, tri1&);
std::istream& operator>>(std::istream& is, tri2&);

class texture {
public:
    virtual ~texture();
    virtual std::uint32_t get_width() const = 0;
    virtual std::uint32_t get_height() const = 0;
};

class Engine {
public:
    // Engine() {}

    virtual std::string initialize() = 0;
    virtual float getTimeFromInit() = 0;
    virtual bool loadTexture(std::string_view) = 0;
    virtual void start() = 0;
    virtual bool input(event& e) = 0;
    virtual void renderTriangle(const triangle&) = 0;

    virtual texture* createTexture(std::string_view path) = 0;
    virtual void destroyTexture(texture* t) = 0;
    virtual void render(const tri0&, const color&) = 0;
    virtual void render(const tri1&) = 0;
    virtual void render(const tri2&, texture*) = 0;
    virtual void render(const tri2&, texture*, const mat2x3& m) = 0;

    virtual void swapBuffers() = 0;
    virtual void uninitialize() = 0;
    virtual ~Engine() {}
};

Engine* create_engine();
void destroy_engine(Engine* e);

//tri0 blend(const tri0& tl, const tri0& tr, const float a);

} // end of namespace grottans
