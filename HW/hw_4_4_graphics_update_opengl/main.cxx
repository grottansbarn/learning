#include "engine.hxx"
#include <cassert>
#include <fstream>
#include <iostream>
#include <math.h>

int main()
{
    using namespace std;

    grottans::Engine* enginereal = grottans::create_engine();

    enginereal->initialize();

    //enginereal->start();
    grottans::texture* texture = enginereal->createTexture("tank.png");
    if (nullptr == texture) {
        cerr << "failed load texture\n";
        cout << "failed load texture\n";
    }

    bool continue_loop = true;
    int current_shader = 0;
    while (continue_loop) {
        grottans::event e;
        while (enginereal->input(e)) {
            cout << e << endl;
            switch (e) {
            case grottans::event::turn_off:
                continue_loop = false;
                break;
            case grottans::event::button1_released:
                ++current_shader;
                if (current_shader > 2) {
                    current_shader = 0;
                }
                break;
            default:
                break;
            }
        }

        if (current_shader == 0) {
            std::ifstream file("vert_pos.txt");
            assert(!!file);

            grottans::tri0 tr1;
            grottans::tri0 tr2;
            grottans::tri0 tr11;
            grottans::tri0 tr22;

            file >> tr1 >> tr2 >> tr11 >> tr22;

            float time = enginereal->getTimeFromInit();
            float alpha = sin(time);

            grottans::tri0 t1 = grottans::blend(tr1, tr11, alpha);
            grottans::tri0 t2 = grottans::blend(tr2, tr22, alpha);

            enginereal->render(t1, grottans::color(1.f, 0.f, 0.f, 1.f));
            enginereal->render(t2, grottans::color(0.f, 1.f, 0.f, 1.f));
        }

        if (current_shader == 1) {
            std::ifstream file("vert_pos_color.txt");
            assert(!!file);

            grottans::tri1 tr1;
            grottans::tri1 tr2;

            file >> tr1 >> tr2;

            enginereal->render(tr1);
            enginereal->render(tr2);
        }

        if (current_shader == 2) {
            std::ifstream file("vert_tex_color.txt");
            assert(!!file);

            grottans::tri2 tr1;
            grottans::tri2 tr2;

            file >> tr1 >> tr2;

            float time = enginereal->getTimeFromInit();
            float s = std::sin(time);
            float c = std::cos(time);

            // animate one triangle texture coordinates
            for (auto& v : tr1.v) {
                v.uv.u += c;
                v.uv.v += s;
            }

            enginereal->render(tr1, texture);
            enginereal->render(tr2, texture);
        }

        enginereal->swapBuffers();
    }

    enginereal->uninitialize();

    return EXIT_SUCCESS;
}
