cmake_minimum_required(VERSION 3.9)
project(4_2_morphing_opengl)

add_executable(${PROJECT_NAME} main.cxx engine.cxx engine.hxx engine_real.hxx fragment_shader.fsh vertex_shader.vsh)
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)

#find_library(SDL2_LIB NAMES libSDL2.a)
#find_library(SNDIO_LIB NAMES libsndio.so)
  
target_link_libraries(${PROJECT_NAME} 
   #"${SDL2_LIB}" # full path to libSDL2.a force to staticaly link with it
   /usr/local/lib/libSDL2.a
   -lm
   -ldl
   -lpthread
   -lrt
   #"${SNDIO_LIB}"
   /usr/lib/x86_64-linux-gnu/libsndio.so
   -lGL
   )
