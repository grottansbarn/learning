#include "engine.hxx"
#include "engine_real.hxx"
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>

#include <algorithm>
#include <array>
#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iostream>
#include <memory>
#include <sstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

//#define GL_FRAGMENT_PRECISION_HIGH 1

PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;

template <typename T>
static void load_gl_func(const char* func_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("can't load GL function") + func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define OM_GL_CHECK()                                                              \
    {                                                                              \
        const GLenum err = glGetError();                                           \
        if (err != GL_NO_ERROR) {                                                  \
            switch (err) {                                                         \
            case GL_INVALID_ENUM:                                                  \
                std::cerr << "GL_INVALID_ENUM" << std::endl;                       \
                break;                                                             \
            case GL_INVALID_VALUE:                                                 \
                std::cerr << "GL_INVALID_VALUE" << std::endl;                      \
                break;                                                             \
            case GL_INVALID_OPERATION:                                             \
                std::cerr << "GL_INVALID_OPERATION" << std::endl;                  \
                break;                                                             \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                                 \
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;      \
                break;                                                             \
            case GL_OUT_OF_MEMORY:                                                 \
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                      \
                break;                                                             \
            }                                                                      \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__ << ')' \
                      << std::endl;                                                \
            assert(false);                                                         \
        }                                                                          \
    }

namespace grottans {

static std::array<std::string_view, 17> event_names = {
    { /// input events
        "left_pressed", "left_released", "right_pressed", "right_released",
        "up_pressed", "up_released", "down_pressed", "down_released",
        "select_pressed", "select_released", "start_pressed", "start_released",
        "button1_pressed", "button1_released", "button2_pressed",
        "button2_released",
        /// virtual console events
        "turn_off" }
};

struct bind {
    bind(SDL_Keycode k, std::string_view s, event pressed, event released)
        : key(k)
        , name(s)
        , event_pressed(pressed)
        , event_released(released)
    {
    }

    SDL_Keycode key;
    std::string_view name;
    event event_pressed;
    event event_released;
};

const std::array<bind, 8> keys{
    { { SDLK_w, "up", event::up_pressed, event::up_released },
        { SDLK_a, "left", event::left_pressed, event::left_released },
        { SDLK_s, "down", event::down_pressed, event::down_released },
        { SDLK_d, "right", event::right_pressed, event::right_released },
        { SDLK_LCTRL, "button1", event::button1_pressed, event::button1_released },
        { SDLK_SPACE, "button2", event::button2_pressed, event::button2_released },
        { SDLK_ESCAPE, "select", event::select_pressed, event::select_released },
        { SDLK_RETURN, "start", event::start_pressed, event::start_released } }
};

std::ostream& operator<<(std::ostream& stream, const event e)
{
    std::uint32_t value = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(event::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_names[value];
        return stream;
    } else {
        throw std::runtime_error("too big event value");
    }
}

std::istream& operator>>(std::istream& is, vertex& v)
{
    is >> v.x;
    is >> v.y;
    return is;
}

std::istream& operator>>(std::istream& is, triangle& t)
{
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

vertex blendVertex(const vertex& vl, const vertex& vr, const float a)
{
    vertex r;
    r.x = (1.0f - a) * vl.x + a * vr.x;
    r.y = (1.0f - a) * vl.y + a * vr.y;
    return r;
}

triangle blendTriangle(const triangle& tl, const triangle& tr, const float a)
{
    triangle r;
    r.v[0] = blendVertex(tl.v[0], tr.v[0], a);
    r.v[1] = blendVertex(tl.v[1], tr.v[1], a);
    r.v[2] = blendVertex(tl.v[2], tr.v[2], a);
    return r;
}

///////////////////////////////////////////////////////////////////////////////
bool check_input(const SDL_Event& e, const bind*& result)
{
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind& b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::initialize
/// \return
///
std::string EngineReal::initialize()
{
    using namespace std;

    stringstream serr;

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: initialization failed: " << err_message << std::endl;
    }

    /*SDL_Window* const */
    window = SDL_CreateWindow("Window on SDL with OpenGL, Keys and Triangle",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640,
        480, ::SDL_WINDOW_OPENGL);
    if (window == nullptr) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: Create Window failed: " << err_message << std::endl;
        SDL_Quit();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// \brief version GL
    ///
    int gl_major_ver = 2;
    int gl_minor_ver = 0;
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    //создаем GL контекст
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("can't create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    if (gl_major_ver < 2) {
        std::clog << "current context opengl version: " << gl_major_ver << '.'
                  << gl_minor_ver << '\n'
                  << "need openg version at least: 2.1\n"
                  << std::flush;
        throw std::runtime_error("opengl version too low");
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
    } catch (std::exception& ex) {
        return ex.what();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// \brief read vertex shader file
    ///
    GLuint vert_shader = glCreateShader(GL_VERTEX_SHADER);
    std::ifstream vertexFile("vertex_shader.vsh",
        std::ios::in | std::ios::binary);
    if (!vertexFile)
        cout << "Shader's file not open.\n";
    std::vector<char> buffer;
    std::ifstream::pos_type size = 0;

    if (vertexFile.seekg(0, std::ios::end)) {
        size = vertexFile.tellg();
    }

    if (size && vertexFile.seekg(0, std::ios::beg)) {
        buffer.resize(static_cast<uint>(size));
        vertexFile.read(&buffer[0], size);
    }
    buffer.push_back('\0');

    string_view vertex_shader_src = buffer.data();
    ///////////////////////////////////////////////////////////////////////////

    const char* source = vertex_shader_src.data();
    glShaderSource(vert_shader, 1, &source, nullptr);
    OM_GL_CHECK();

    glCompileShader(vert_shader);
    OM_GL_CHECK();

    GLint compiled_status = 0;
    glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &compiled_status);

    //check vertex compilation
    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(vert_shader, GL_INFO_LOG_LENGTH, &info_len);
        OM_GL_CHECK();

        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(vert_shader, info_len, nullptr, info_chars.data());
        OM_GL_CHECK();

        glDeleteShader(vert_shader);
        OM_GL_CHECK();

        std::string shader_type_name = "vertex";
        serr << "Error compiling shader(vertex)\n"
             << vertex_shader_src << "\n"
             << info_chars.data();
        cout << serr.str();
        // return serr.str();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// \brief read fragment shader file
    ///
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    std::ifstream fragmentFile("fragment_shader.fsh",
        std::ios::in | std::ios::binary);
    if (!fragmentFile)
        cout << "Shader's file not open.\n";
    std::vector<char> f_buffer;
    std::ifstream::pos_type f_size = 0;

    if (fragmentFile.seekg(0, std::ios::end)) {
        f_size = fragmentFile.tellg();
    }

    if (f_size && fragmentFile.seekg(0, std::ios::beg)) {
        f_buffer.resize(static_cast<uint>(f_size));
        fragmentFile.read(&f_buffer[0], f_size);
    }
    f_buffer.push_back('\0');

    string_view fragment_shader_src = f_buffer.data();
    //////////////////////

    source = fragment_shader_src.data();
    glShaderSource(fragment_shader, 1, &source, nullptr);
    OM_GL_CHECK();
    glCompileShader(fragment_shader);
    OM_GL_CHECK();
    compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK();

    //check fragment compilation
    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);

        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(fragment_shader, info_len, nullptr, info_chars.data());

        glDeleteShader(fragment_shader);

        serr << "Error compiling shader(fragment)\n"
             << fragment_shader_src << "\n"
             << info_chars.data();

        // return serr.str();
        cout << serr.str();
    }

    ///////////////////////////////////////////////////////////////////////////
    // now create program and attach vertex and fragment shaders
    program_id_ = glCreateProgram();
    OM_GL_CHECK();
    if (0 == program_id_) {
        serr << "failed to create gl program";
        return serr.str();
    }

    glAttachShader(program_id_, vert_shader);
    OM_GL_CHECK();

    glAttachShader(program_id_, fragment_shader);
    OM_GL_CHECK();

    // bind attribute location
    glBindAttribLocation(program_id_, 0, "a_position");
    OM_GL_CHECK();

    // link program after binding attribute locations
    glLinkProgram(program_id_);
    OM_GL_CHECK();

    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    OM_GL_CHECK();

    if (linked_status == 0) {
        GLint infoLen = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
        OM_GL_CHECK();

        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
        OM_GL_CHECK();

        serr << "Error linking program:\n"
             << infoLog.data();
        glDeleteProgram(program_id_);
        OM_GL_CHECK();
        //return serr.str();
        cout << serr.str();
    }

    // turn on rendering with just created
    // shader program
    glUseProgram(program_id_);
    OM_GL_CHECK();

    glEnable(GL_DEPTH_TEST);

    return "";
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::input
///
bool EngineReal::input(event& e)
{
    using namespace std;
    while (true) {
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) { //waiting event
            const bind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT) {
                e = grottans::event::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_pressed; //????????????????????????????
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_released; //????????????????????????????
                    return true;
                }
            }
        }
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::renderTriangle
///
void EngineReal::renderTriangle(const triangle& t)
{
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), &t.v[0]);
    OM_GL_CHECK();
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();
    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief swap_buffers
///
void EngineReal::swapBuffers()
{
    SDL_GL_SwapWindow(window);
    glClearColor(0.0f, 1.0f, 1.0f, 0.0f);
    OM_GL_CHECK();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::start
///
void EngineReal::start()
{
    std::ifstream file("vertexes.txt", std::ios_base::binary);
    assert(!!file);

    triangle tr1q;
    triangle tr2q;

    triangle tr1t;
    triangle tr2t;

    file >> tr1q >> tr2q >> tr1t >> tr2t;

    bool continue_loop = true;
    while (continue_loop) {
        event e;
        while (input(e)) {
            std::cout << e << std::endl;
            switch (e) {
            case grottans::event::turn_off:
                continue_loop = false;
                break;
            default:
                break;
            }
        }

        float alpha = (std::sin(this->getTimeFromInit()) * 0.5f) + 0.5f;

        triangle tr1 = blendTriangle(tr1q, tr1t, alpha);
        triangle tr2 = blendTriangle(tr2q, tr2t, alpha);

        this->renderTriangle(tr1);
        this->renderTriangle(tr2);

        this->swapBuffers();
    }
}
///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::getTimeFromInit
/// \return
///
float EngineReal::getTimeFromInit()
{
    std::uint32_t ms_from_library_initialization = SDL_GetTicks();
    float seconds = ms_from_library_initialization * 0.001f;
    return seconds;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::uninitialize
///
void EngineReal::uninitialize()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    destroy_engine(this);
}

// EngineReal::~EngineReal() {}

///////////////////////////////////////////////////////////////////////////////
static bool already_exist = false;

EngineReal* createEngine()
{
    if (already_exist) {
        throw std::runtime_error("engine already exist");
    }
    EngineReal* result = new EngineReal();
    already_exist = true;
    return result;
}

void destroy_engine(EngineReal* e)
{
    if (already_exist == false) {
        throw std::runtime_error("engine not created");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr");
    }
    delete e;
}

} // end of namespace grottans
