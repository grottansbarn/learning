#pragma once

#include <string>

namespace grottans {

enum class event {
    // input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button1_pressed,
    button1_released,
    button2_pressed,
    button2_released,
    turn_off
};

std::ostream& operator<<(std::ostream& stream, const event e);

struct vertex {
    vertex()
        : x(0.f)
        , y(0.f)
    {
    }
    float x;
    float y;
};

struct triangle {
    triangle()
    {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

std::istream& operator>>(std::istream& is, vertex&);
std::istream& operator>>(std::istream& is, triangle&);

class Engine;

class Engine {
public:
    // Engine() {}

    virtual std::string initialize() = 0;
    virtual float getTimeFromInit() = 0;
    virtual void start() = 0;
    virtual bool input(event& e) = 0;
    virtual void renderTriangle(const triangle&) = 0;
    virtual void swapBuffers() = 0;
    virtual void uninitialize() = 0;
    virtual ~Engine() {}
};

} // end of namespace grottans
