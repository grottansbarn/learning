#include "engine.hxx"
#include <SDL2/SDL.h>

namespace grottans {

class EngineReal : public Engine {
    SDL_Window* window = nullptr;

public:
    std::string initialize();
    void start();
    bool input(event& e);
    void uninitialize();
    ~EngineReal() {}
};

EngineReal* create_engine();
void destroy_engine(EngineReal* e);
} //end of namespace grottans
