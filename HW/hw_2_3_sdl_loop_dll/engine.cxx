#include "engine.hxx"
#include "enginereal.hxx"
#include <SDL2/SDL.h>

#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>

namespace grottans {

static std::array<std::string_view, 17> event_names = {
    { /// input events
        "left_pressed", "left_released", "right_pressed", "right_released",
        "up_pressed", "up_released", "down_pressed", "down_released",
        "select_pressed", "select_released", "start_pressed", "start_released",
        "button1_pressed", "button1_released", "button2_pressed",
        "button2_released",
        /// virtual console events
        "turn_off" }
};

struct bind {
    bind(SDL_Keycode k, std::string_view s, event pressed, event released)
        : key(k)
        , name(s)
        , event_pressed(pressed)
        , event_released(released)
    {
    }

    SDL_Keycode key;
    std::string_view name;
    event event_pressed;
    event event_released;
};

const std::array<bind, 8> keys{
    { { SDLK_w, "up", event::up_pressed, event::up_released },
        { SDLK_a, "left", event::left_pressed, event::left_released },
        { SDLK_s, "down", event::down_pressed, event::down_released },
        { SDLK_d, "right", event::right_pressed, event::right_released },
        { SDLK_LCTRL, "button1", event::button1_pressed,
            event::button1_released },
        { SDLK_SPACE, "button2", event::button2_pressed,
            event::button2_released },
        { SDLK_ESCAPE, "select", event::select_pressed, event::select_released },
        { SDLK_RETURN, "start", event::start_pressed, event::start_released } }
};

std::ostream& operator<<(std::ostream& stream, const event e)
{
    std::uint32_t value = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(event::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_names[value];
        return stream;
    } else {
        throw std::runtime_error("too big event value");
    }
}

///////////////////////////////////////////////////////////////////////////////
bool check_input(const SDL_Event& e, const bind*& result)
{
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind& b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::initialize
/// \return
///
std::string EngineReal::initialize()
{
    using namespace std;

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: initialization failed: " << err_message << std::endl;
    }

    /*SDL_Window* const */
    window = SDL_CreateWindow(
        "First window on SDL with keys", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);
    if (window == nullptr) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: Create Window failed: " << err_message << std::endl;
        SDL_Quit();
    }
    return "";
}

/// pool event from input queue
/// return true if more events in queue
bool EngineReal::input(event& e)
{
    using namespace std;
    while (true) {
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) { //ждем ивента сдл
            const bind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT) {
                e = grottans::event::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_pressed; //????????????????????????????
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_released; //????????????????????????????
                    return true;
                }
            }
        }
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::start
///
void EngineReal::start()
{
    bool continue_loop = true;
    while (continue_loop) {
        event e;
        while (input(e)) {
            std::cout << e << std::endl;
            switch (e) {
            case grottans::event::turn_off:
                continue_loop = false;
                break;
            default:
                break;
            }
        }
    }
}

/*
///////////////////////////////////////////////////////////////////////////////
/// \brief check_input
/// \param e
///
void EngineReal::checkInput(const event& e)
{
    using namespace std;

    //алгоритмом find_it находим и возвращаем указатель на элемент типа bind из
    // keys, которая удовлетворяет условию, реализованному лямбдой
    //(сравнение клавиш из keys с клавишей из ивента &e)
    // const auto it = find_if(
    //     begin(keys), end(keys),
    //     [&](const grottans::bind& b) { return b[i].key == e; });
    const auto it = end(keys);
    for (bind x : keys) {
        if (x.event_pressed == e || x.event_pressed == e)
            it = x;
    }

    //вывод результата поиска
    if (it != end(keys)) {
        cout << it->name << ' ';
        if (e.type == SDL_KEYDOWN) {
            cout << "is pressed" << endl;
        } else {
            cout << "is released" << endl;
        }
    } else { //если find_it не находит клавишу
        cout << "Unknown key used!" << endl;
    }
}
*/
///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::uninitialize
///
void EngineReal::uninitialize()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
    destroy_engine(this);
}

//EngineReal::~EngineReal() {}

///////////////////////////////////////////////////////////////////////////////
static bool already_exist = false;

EngineReal* create_engine()
{
    if (already_exist) {
        throw std::runtime_error("engine already exist");
    }
    EngineReal* result = new EngineReal();
    already_exist = true;
    return result;
}

void destroy_engine(EngineReal* e)
{
    if (already_exist == false) {
        throw std::runtime_error("engine not created");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr");
    }
    delete e;
}

} // end of namespace grottans
