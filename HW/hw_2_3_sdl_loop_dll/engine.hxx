#pragma once

#include <string>

namespace grottans {

enum class event {
    // input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button1_pressed,
    button1_released,
    button2_pressed,
    button2_released,
    turn_off
};

class Engine;

class Engine {
public:
    //Engine() {}

    virtual std::string initialize() = 0;
    virtual void start() = 0;
    virtual bool input(event& e) = 0;
    virtual void uninitialize() = 0;
    virtual ~Engine() {}
};

std::ostream& operator<<(std::ostream& stream, const event e);

} // end of namespace grottans

///////////////////////////////////////////////////////////////////////////////
/// todo
/// - запрет запуска 2х экземпляров движка
/// - engine_implement Для абстракции к фреймворку на котором реализуются
/// функции движка
///
