cmake_minimum_required(VERSION 3.5)
project(hello-lib)

# select SHARED or STATIC library
add_library(hello-lib STATIC include/hello_lib.hxx src/hello_lib.cxx)
target_include_directories(hello-lib PUBLIC 
                          ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_compile_features(hello-lib PUBLIC cxx_std_17)
