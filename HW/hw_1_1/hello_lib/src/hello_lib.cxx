#include <iostream>
//#include <string_view>
#include <cstdlib>

bool greetings(const char* output_phrase)
{
    using namespace std;
    const char* user_name = std::getenv("USER");
    cout << output_phrase << user_name << endl;
    return cout.good();
}
