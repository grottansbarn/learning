cmake_minimum_required(VERSION 3.9)
project(hw_2_1_sdl_dynamic)

add_executable(${PROJECT_NAME} main.cxx)
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)

find_library(SDL2_LIB libSDL2.so)

# find out what libraries are needed for dynamicaly linking with libSDL.so
# using default linux compiler
#$> sdl2-config --static-libs
#-lSDL2
    
target_link_libraries(${PROJECT_NAME} 
       -lSDL2
       )

