#pragma once

#include <array>
//#include <cmath>
#include <cstddef>
#include <fstream>
#include <iomanip>
#include <vector>

//constexpr double t = 0.1;
constexpr size_t width = 320;
constexpr size_t height = 240;

const size_t buffer_size = width * height;

#pragma pack(push, 1)
struct color {
    color(uint8_t _r, uint8_t _g, uint8_t _b)
    {
        r = _r;
        g = _g;
        b = _b;
    }
    uint8_t r = 255;
    uint8_t g = 255;
    uint8_t b = 255;
};
#pragma pack(pop)

struct position {
    position() = default;
    position(int32_t _x, int32_t _y)
    {
        x = _x;
        y = _y;
    }
    int32_t x = 0;
    int32_t y = 0;
};

struct vertex {
    //vertex() = default;
    vertex(double _x = 0, double _y = 0, double _r = 0, double _g = 0,
        double _b = 0, double _x1 = 0, double _y1 = 0, double _f = 0)
    {
        x = _x; //x
        y = _y; //y
        r = _r; //r
        g = _g; //g
        b = _b; //b
        x1 = _x1; //x'
        y1 = _y1; //y'
        f = _f; //null
    }
    double x = 0; /// x
    double y = 0; /// y
    double r = 0; /// r
    double g = 0; /// g
    double b = 0; /// b
    double x1 = 0; /// u (texture coordinate)
    double y1 = 0; /// v (texture coordinate)
    double f = 0; /// ?
};

double interpolate(const double& a, const double& b, const double& t)
{
    return a + (b - a) * t;
}

vertex interpolate(const vertex& v1, const vertex& v2, const double& t)
{
    return {
        interpolate(v1.x, v2.x, t),
        interpolate(v1.y, v2.y, t),
        interpolate(v1.r, v2.r, t),
        interpolate(v1.g, v2.g, t),
        interpolate(v1.b, v2.b, t),
        interpolate(v1.x1, v2.x1, t),
        interpolate(v1.y1, v2.y1, t),
        interpolate(v1.f, v2.f, t)
    };
}

//using pixels = std::vector<position>;

void saveImage(const std::string& file_name, const std::vector<color>& image)
{
    std::ofstream out_file;
    out_file.exceptions(std::ios_base::failbit);
    out_file.open(file_name, std::ios_base::binary);
    out_file << "P6\n"
             << width << ' ' << height << ' ' << 255 << '\n';
    for (uint i = 0; i < buffer_size; i++) {
        out_file << static_cast<const int8_t>(image[i].r);
        out_file << static_cast<const int8_t>(image[i].g);
        out_file << static_cast<const int8_t>(image[i].b);
    }
}
