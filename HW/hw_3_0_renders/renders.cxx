#include "renders.hxx"
#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <vector>

///////////////////////////////////////////////////////////////////////////////
/// \brief The Render class
///
class Render {
public:
    int w = width;
    int h = height;
    std::vector<position> buffer;
    std::vector<color> renderImage;

    void imageInit(color c)
    {
        //заливаем полотно заданным цветом
        for (int i = 0; i < w * h; i++)
            renderImage.push_back(c);
    }

    void count(position p1, position p2)
    {
        // buffer.clear();

        int32_t x0 = p1.x;
        int32_t y0 = p1.y;
        int32_t x1 = p2.x;
        int32_t y1 = p2.y;
        //написано для одного квардранта
        //затем сперто из примера, больно уж лямбды понравились
        auto plot_line_low = [&](int x0, int y0, int x1, int y1) {
            int dx = x1 - x0;
            int dy = y1 - y0;
            int yi = 1;
            if (dy < 0) {
                yi = -1;
                dy = -dy;
            }
            int D = 2 * dy - dx;
            int y = y0;

            for (int x = x0; x <= x1; ++x) {
                buffer.push_back(position{ x, y });
                if (D > 0) {
                    y += yi;
                    D -= 2 * dx;
                }
                D += 2 * dy;
            }
        };

        auto plot_line_high = [&](int x0, int y0, int x1, int y1) {
            int dx = x1 - x0;
            int dy = y1 - y0;
            int xi = 1;
            if (dx < 0) {
                xi = -1;
                dx = -dx;
            }
            int D = 2 * dx - dy;
            int x = x0;

            for (int y = y0; y <= y1; ++y) {
                buffer.push_back(position{ x, y });
                if (D > 0) {
                    x += xi;
                    D -= 2 * dy;
                }
                D += 2 * dx;
            }
        };

        if (abs(y1 - y0) < abs(x1 - x0)) {
            if (x0 > x1) {
                plot_line_low(x1, y1, x0, y0);
            } else {
                plot_line_low(x0, y0, x1, y1);
            }
        } else {
            if (y0 > y1) {
                plot_line_high(x1, y1, x0, y0);
            } else {
                plot_line_high(x0, y0, x1, y1);
            }
        }
    }

    void draw(color c)
    {
        //для каждой position из buffer
        //определяем соответствующую точку в линейном векторе renderImage
        //и заливаем в нее нужный цвет
        for (position x : buffer) {
            uint index = static_cast<uint>(x.y * w + x.x);
            renderImage[index].r = c.r;
            renderImage[index].g = c.g;
            renderImage[index].b = c.b;
        }
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief The TrRender class
///
class TrRender : public Render {
public:
    //хранит вершины треугольника тройками
    std::vector<position> triangle;

    void build()
    {
        //строим три грани треугольника
        count(triangle[0], triangle[1]);
        count(triangle[1], triangle[2]);
        count(triangle[2], triangle[0]);
        triangle.clear();
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief The IndexRender class
///
class IndexRender : public TrRender {
public:
    std::vector<position> apexes; //вершины
    std::vector<uint8_t> indexes; //индексы

    void indexing()
    {
        //занесенные в apexes и indexes вершины складываем в вектор тройками
        for (uint i = 0; i < static_cast<uint>(indexes.size()); i += 3) {
            triangle.push_back(apexes[indexes[i]]);
            triangle.push_back(apexes[indexes[i + 1]]);
            triangle.push_back(apexes[indexes[i + 2]]);

            build();
        }
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief The InterpolatedRender class
///
class InterpolatedRender : public IndexRender {
public:
    std::vector<vertex> vertexes;

    void rasterHorizontalLine(const vertex& left, const vertex& right)
    {
        //vertexes.clear();
        double numberXcoordinates = fabs(right.x - left.x);
        if (numberXcoordinates > 0) {
            for (double i = 0; i <= numberXcoordinates; i++) {
                double t = i / numberXcoordinates;
                vertexes.push_back(interpolate(left, right, t));
            }
        } else {
            double t = 0;
            vertexes.push_back(interpolate(left, right, t));
        }
    }

    void rasterHorizontalTriangle(const vertex& top,
        const vertex& left, const vertex& right)
    {
        double numberHorizontalLines = fabs(left.y - top.y);

        if (numberHorizontalLines > 0) {
            for (double i = 0; i <= numberHorizontalLines; i++) {
                double t = i / numberHorizontalLines;
                vertex left_t = interpolate(top, left, t);
                vertex right_t = interpolate(top, right, t);
                rasterHorizontalLine(left_t, right_t);
            }
        } else {
            rasterHorizontalLine(left, right);
        }
    }

    void rasterTriangle(const vertex& v1, const vertex& v2, const vertex& v3)
    {
        //ищем вершину, у которой игрек между другими двумя
        //после долгих экспериментов подсмотрено в примере
        std::array<vertex, 3> sorting;
        sorting[0] = v1;
        sorting[1] = v2;
        sorting[2] = v3;
        std::sort(begin(sorting), end(sorting),
            [](const vertex left, const vertex right) {
                return left.y > right.y;
            });

        //переобозначаем вершины
        vertex top = sorting[0];
        vertex middle = sorting[1];
        vertex down = sorting[2];

        vertex middleHorizont;
        //интерполируем точку по грани и известному игреку
        double t = (top.y - middle.y) / (top.y - down.y);
        middleHorizont = interpolate(top, down, t);

        rasterHorizontalTriangle(top, middle, middleHorizont);
        rasterHorizontalTriangle(down, middle, middleHorizont);
    }

    void drawRaster()
    {
        //заливаем цвет из растеризованных пикселей в renderImage для сохранения
        for (vertex x : vertexes) {
            uint index = static_cast<uint>(x.y * width + x.x);
            renderImage[index].r = static_cast<uint8_t>(x.r);
            renderImage[index].g = static_cast<uint8_t>(x.g);
            renderImage[index].b = static_cast<uint8_t>(x.b);
        }
    }

    void clear()
    {
        vertexes.clear();
        indexes.clear();
        apexes.clear();
        triangle.clear();
        buffer.clear();
        renderImage.clear();
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief The AlmightyRender class
///
class AlmightyRender : public InterpolatedRender {
public:
    //поворот на угол, зуммирование, смещение относительно первой вершины
    void calculation(const vertex& _v1, const vertex& _v2, const vertex& _v3,
        const double& angle = 0.0, const double& zoom = 1,
        const int& moveX = 0, const int& moveY = 0)
    {
        vertex v1 = _v1;
        vertex v2 = _v2;
        vertex v3 = _v3;

        // зум относительно v1
        if (zoom != 1.0) {
            v2.x = interpolate(v1.x, v2.x, zoom);
            v2.y = interpolate(v1.y, v2.y, zoom);
            v3.x = interpolate(v1.x, v3.x, zoom);
            v3.y = interpolate(v1.y, v3.y, zoom);
        }

        // поворот относительно центра фигуры
        // при наличии 2х вершин с равными иксами начинает гадить координаты,
        //не могу отловить
        /*
        double x12 = v1.x - v2.x;
        double x23 = v2.x - v3.x;
        double x31 = v3.x - v1.x;
        double y12 = v1.y - v2.y;
        double y23 = v2.y - v3.y;
        double y31 = v3.y - v2.y;
        double z1 = v1.x * v1.x + v1.y * v1.y;
        double z2 = v2.x * v2.x + v2.y * v2.y;
        double z3 = v3.x * v3.x + v3.y * v3.y;
        double zx = y12 * z3 + y23 * z1 + y31 * z2;
        double zy = x12 * z3 + x23 * z1 + x31 * z2;
        double z = x12 * y31 - y12 * x31;
        double x_centr = -(zx / (2 * z));
        double y_centr = zy / (2 * z);
        */
        double x_centr = (v1.x + v2.x + v3.x) / 3;
        double y_centr = (v1.y + v2.y + v3.y) / 3;
        double alpha = angle * 3.1415 / 180;
        double cos_a = std::cos(alpha);
        double sin_a = std::sin(alpha);
        double v1x = cos_a * (v1.x - x_centr) - sin_a * (v1.y - y_centr) + x_centr;
        double v1y = sin_a * (v1.x - x_centr) + cos_a * (v1.y - y_centr) + y_centr;
        double v2x = cos_a * (v2.x - x_centr) - sin_a * (v2.y - y_centr) + x_centr;
        double v2y = sin_a * (v2.x - x_centr) + cos_a * (v2.y - y_centr) + y_centr;
        double v3x = cos_a * (v3.x - x_centr) - sin_a * (v3.y - y_centr) + x_centr;
        double v3y = sin_a * (v3.x - x_centr) + cos_a * (v3.y - y_centr) + y_centr;
        v1.x = v1x;
        v1.y = v1y;
        v2.x = v2x;
        v2.y = v2y;
        v3.x = v3x;
        v3.y = v3y;

        //смещение
        v1.x += moveX;
        v2.x += moveX;
        v3.x += moveX;
        v1.y += moveY;
        v2.y += moveY;
        v3.y += moveY;

        rasterTriangle(v1, v2, v3);
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief main
/// \return
///
int main()
{
    const color black = { 0, 0, 0 };
    const color white = { 255, 255, 255 };
    const color green = { 0, 255, 0 };

    ///////////////////////////////////////////////////////////////////////////
    Render render;
    render.imageInit(white);

    for (size_t i = 0; i <= 10; ++i) {
        position start{ rand() % static_cast<int>(width - 1),
            rand() % static_cast<int>(height - 1) };
        position end{ rand() % static_cast<int>(width - 1),
            rand() % static_cast<int>(height - 1) };
        color color{ static_cast<uint8_t>(rand() % 256),
            static_cast<uint8_t>(rand() % 256),
            static_cast<uint8_t>(rand() % 256) };

        render.count(start, end);

        render.draw(color);

        render.buffer.clear();
    }

    saveImage("01_lines.ppm", render.renderImage);

    ///////////////////////////////////////////////////////////////////////////
    TrRender tr;
    tr.imageInit(white);

    tr.triangle.push_back({ 0, 0 });
    tr.triangle.push_back({ 319, 0 });
    tr.triangle.push_back({ 319, 239 });
    tr.build();
    tr.draw(green);
    //tr.triangle.clear();

    tr.triangle.push_back({ 0, 0 });
    tr.triangle.push_back({ 0, 239 });
    tr.triangle.push_back({ 319, 239 });

    tr.build();

    tr.draw(green);

    saveImage("02_triangle.ppm", tr.renderImage);

    ///////////////////////////////////////////////////////////////////////////
    TrRender trrender;
    trrender.imageInit(white);

    for (size_t i = 0; i <= 10; ++i) {
        trrender.triangle.clear();
        trrender.triangle.push_back({ rand() % static_cast<int>(width - 1),
            rand() % static_cast<int>(height - 1) });
        trrender.triangle.push_back({ rand() % static_cast<int>(width - 1),
            rand() % static_cast<int>(height - 1) });
        trrender.triangle.push_back({ rand() % static_cast<int>(width - 1),
            rand() % static_cast<int>(height - 1) });
        color color{ static_cast<uint8_t>(rand() % 256),
            static_cast<uint8_t>(rand() % 256),
            static_cast<uint8_t>(rand() % 256) };

        trrender.build();

        trrender.draw(color);

        trrender.buffer.clear();
    }

    saveImage("02_triangles.ppm", trrender.renderImage);

    ///////////////////////////////////////////////////////////////////////////
    TrRender TrRenderGrid;
    TrRenderGrid.imageInit(white);

    size_t maxX = 5;
    size_t maxY = 5;

    int32_t step_x = (width - 1) / maxX;
    int32_t step_y = (height - 1) / maxY;

    for (size_t i = 0; i < maxX; ++i) {
        for (size_t j = 0; j < maxY; ++j) {

            TrRenderGrid.triangle.clear();

            position p0(0 + static_cast<int>(i) * step_x,
                0 + static_cast<int>(j) * step_y);
            position p1(p0.x + step_x, p0.y + step_y);
            position p2(p0.x, p0.y + step_y);
            position p3(p0.x + step_x, p0.y);

            TrRenderGrid.triangle.push_back(p0);
            TrRenderGrid.triangle.push_back(p1);
            TrRenderGrid.triangle.push_back(p2);

            TrRenderGrid.build();

            TrRenderGrid.triangle.push_back(p0);
            TrRenderGrid.triangle.push_back(p3);
            TrRenderGrid.triangle.push_back(p1);

            TrRenderGrid.build();
        }
    }

    TrRenderGrid.draw(green);

    saveImage("03_triangles_grid.ppm", TrRenderGrid.renderImage);

    ///////////////////////////////////////////////////////////////////////////
    IndexRender IndexRender;
    IndexRender.imageInit(white);

    // заливаем все вершины в вектор
    for (size_t i = 0; i <= maxY; ++i) {
        for (size_t j = 0; j <= maxX; ++j) {
            IndexRender.apexes.push_back({ static_cast<int32_t>(j) * step_x,
                static_cast<int32_t>(i) * step_y });
        }
    }

    //цикл объединения вершин в тройки по индексам
    //(нагло спёрто и подправлено ибо это набивка для конкретной сетки)
    for (size_t x = 0; x < maxY; ++x) {
        for (size_t y = 0; y < maxX; ++y) {
            //ручное индексирование кучи треугольников
            //думаю это та еще проблема при обработке модели
            uint8_t index0 = static_cast<uint8_t>(x * (maxX + 1) + y);
            uint8_t index1 = static_cast<uint8_t>(index0 + (maxX + 1) + 1);
            uint8_t index2 = index1 - 1;
            uint8_t index3 = index0 + 1;

            //заливаем в вектор индексов вершины двух треугольников тройками
            IndexRender.indexes.push_back(index0);
            IndexRender.indexes.push_back(index1);
            IndexRender.indexes.push_back(index2);

            IndexRender.indexes.push_back(index0);
            IndexRender.indexes.push_back(index3);
            IndexRender.indexes.push_back(index1);
        }
    }

    IndexRender.indexing();

    IndexRender.draw(green);

    saveImage("04_triangles_grid_index.ppm", IndexRender.renderImage);

    ///////////////////////////////////////////////////////////////////////////
    InterpolatedRender InterpolatedRender;

    InterpolatedRender.imageInit(black);
    InterpolatedRender.rasterTriangle({ 80, 100, 255, 0, 0 },
        { 100, 200, 0, 255, 0 }, { 200, 160, 0, 0, 255 });
    InterpolatedRender.drawRaster();
    saveImage("05_interpolated.ppm", InterpolatedRender.renderImage);
    InterpolatedRender.clear();

    InterpolatedRender.imageInit(white);
    InterpolatedRender.rasterTriangle({ 200, 50, 255, 0, 0 },
        { 50, 125, 0, 255, 0 }, { 200, 200, 0, 0, 255 });
    InterpolatedRender.drawRaster();
    saveImage("06_interpolated.ppm", InterpolatedRender.renderImage);
    InterpolatedRender.clear();

    ///////////////////////////////////////////////////////////////////////////
    AlmightyRender AlmightyRender1;
    AlmightyRender1.imageInit(white);
    //рисуем треугольник с предыдущей картинки
    //уменьшенный в 2 раза, повернутый на 90 градусов и смещенный на 50,50 пикселей
    AlmightyRender1.calculation({ 200, 50, 255, 0, 0 },
        { 50, 125, 0, 255, 0 }, { 200, 200, 0, 0, 255 }, 90.0, 0.5, 50, 50);
    AlmightyRender1.drawRaster();
    saveImage("07_transformated.ppm", AlmightyRender1.renderImage);
    AlmightyRender1.clear();

    ///////////////////////////////////////////////////////////////////////////
    /// \brief try load texture
    ///
    AlmightyRender Texture;
    //берем сетку из треугольников отрисованную индексированным рендером
    //и накладываем на нее крученный-верченный заданный треугольник
    Texture.renderImage = IndexRender.renderImage;
    Texture.calculation({ 200, 50, 255, 0, 0 },
        { 50, 125, 0, 255, 0 }, { 200, 200, 0, 0, 255 }, 90.0, 0.5, 50, 50);
    Texture.drawRaster();
    saveImage("08_textured.ppm", Texture.renderImage);
    Texture.clear();
    return EXIT_SUCCESS;
}
