#include "engine.hxx"
#include "picopng.hxx"

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <vector>

static PFNGLCREATESHADERPROC glCreateShader = nullptr;
static PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
static PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
static PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
static PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
static PFNGLDELETESHADERPROC glDeleteShader = nullptr;
static PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
static PFNGLATTACHSHADERPROC glAttachShader = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
static PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
static PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
static PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
static PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
static PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
static PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = nullptr;
static PFNGLUNIFORM1IPROC glUniform1i = nullptr;
static PFNGLACTIVETEXTUREPROC glActiveTextureMY = nullptr;
static PFNGLUNIFORM4FVPROC glUniform4fv = nullptr;

template <typename T>
static void load_gl_func(const char* func_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("can't load GL function") + func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define OM_GL_CHECK()                                                              \
    {                                                                              \
        const GLenum err = glGetError();                                           \
        if (err != GL_NO_ERROR) {                                                  \
            switch (err) {                                                         \
            case GL_INVALID_ENUM:                                                  \
                std::cerr << "GL_INVALID_ENUM" << std::endl;                       \
                break;                                                             \
            case GL_INVALID_VALUE:                                                 \
                std::cerr << "GL_INVALID_VALUE" << std::endl;                      \
                break;                                                             \
            case GL_INVALID_OPERATION:                                             \
                std::cerr << "GL_INVALID_OPERATION" << std::endl;                  \
                break;                                                             \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                                 \
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;      \
                break;                                                             \
            case GL_OUT_OF_MEMORY:                                                 \
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                      \
                break;                                                             \
            }                                                                      \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__ << ')' \
                      << std::endl;                                                \
            assert(false);                                                         \
        }                                                                          \
    }

namespace grottans {

texture::~texture() {}

///////////////////////////////////////////////////////////////////////////////
/// \brief The texture_gl_es20 class
///
class texture_gl_es20 : public texture {
public:
    explicit texture_gl_es20(std::string_view path);
    ~texture_gl_es20() override;

    void bind() const
    {
        glBindTexture(GL_TEXTURE_2D, tex_handl);
        OM_GL_CHECK();
    }

    std::uint32_t get_width() const final { return width; }
    std::uint32_t get_height() const final { return height; }

private:
    std::string file_path;
    GLuint tex_handl = 0;
    std::uint32_t width = 0;
    std::uint32_t height = 0;
};

///////////////////////////////////////////////////////////////////////////////
/// \brief The shader_gl_es20 class
///
class shader_gl_es20 {
public:
    shader_gl_es20( //constructor
        std::string_view vertex_src, std::string_view fragment_src,
        const std::vector<std::tuple<GLuint, const GLchar*> >& attributes)
    {
        vert_shader = compile_shader(GL_VERTEX_SHADER, vertex_src);
        frag_shader = compile_shader(GL_FRAGMENT_SHADER, fragment_src);
        if (vert_shader == 0 || frag_shader == 0) {
            throw std::runtime_error("can't compile shader");
        }
        program_id = link_shader_program(attributes);
        if (program_id == 0) {
            throw std::runtime_error("can't link shader");
        }
    }

    void use() const
    {
        glUseProgram(program_id);
        OM_GL_CHECK();
    }

    void set_uniform(std::string_view uniform_name, texture_gl_es20* texture)
    {
        assert(texture != nullptr);
        const int location = glGetUniformLocation(program_id, uniform_name.data());
        OM_GL_CHECK();
        if (location == -1) {
            std::cerr << "can't get uniform location from shader\n";
            throw std::runtime_error("can't get uniform location");
        }
        unsigned int texture_unit = 0;
        glActiveTextureMY(GL_TEXTURE0 + texture_unit);
        OM_GL_CHECK();

        texture->bind();

        // http://www.khronos.org/opengles/sdk/docs/man/xhtml/glUniform.xml
        glUniform1i(location, static_cast<int>(0 + texture_unit));
        OM_GL_CHECK();
    }

    void set_uniform(std::string_view uniform_name, const color& c)
    {
        const int location = glGetUniformLocation(program_id, uniform_name.data());
        OM_GL_CHECK();
        if (location == -1) {
            std::cerr << "can't get uniform location from shader\n";
            throw std::runtime_error("can't get uniform location");
        }
        float values[4] = { c.get_r(), c.get_g(), c.get_b(), c.get_a() };
        glUniform4fv(location, 1, &values[0]);
        OM_GL_CHECK();
    }

private:
    GLuint compile_shader(GLenum shader_type, std::string_view src)
    {
        GLuint shader_id = glCreateShader(shader_type);
        OM_GL_CHECK();
        std::string_view vertex_shader_src = src;
        const char* source = vertex_shader_src.data();
        glShaderSource(shader_id, 1, &source, nullptr);
        OM_GL_CHECK();

        glCompileShader(shader_id);
        OM_GL_CHECK();

        GLint compiled_status = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
        OM_GL_CHECK();
        if (compiled_status == 0) {
            GLint info_len = 0;
            glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
            OM_GL_CHECK();
            std::vector<char> info_chars(static_cast<size_t>(info_len));
            glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data()); //NULL
            OM_GL_CHECK();
            glDeleteShader(shader_id);
            OM_GL_CHECK();

            std::string shader_type_name = shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
            std::cerr << "Error compiling shader(vertex)\n"
                      << vertex_shader_src << "\n"
                      << info_chars.data();
            return 0;
        }
        return shader_id;
    }

    GLuint link_shader_program(
        const std::vector<std::tuple<GLuint, const GLchar*> >& attributes)
    {
        GLuint program_id_ = glCreateProgram();
        OM_GL_CHECK();
        if (0 == program_id_) {
            std::cerr << "failed to create gl program";
            throw std::runtime_error("can't link shader");
        }

        glAttachShader(program_id_, vert_shader);
        OM_GL_CHECK();
        glAttachShader(program_id_, frag_shader);
        OM_GL_CHECK();

        // bind attribute location
        for (const auto& attr : attributes) {
            GLuint loc = std::get<0>(attr);
            const GLchar* name = std::get<1>(attr);
            glBindAttribLocation(program_id_, loc, name);
            OM_GL_CHECK();
        }

        // link program after binding attribute locations
        glLinkProgram(program_id_);
        OM_GL_CHECK();
        // Check the link status
        GLint linked_status = 0;
        glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
        OM_GL_CHECK();
        if (linked_status == 0) {
            GLint infoLen = 0;
            glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
            OM_GL_CHECK();
            std::vector<char> infoLog(static_cast<size_t>(infoLen));
            glGetProgramInfoLog(program_id_, infoLen, NULL, infoLog.data());
            OM_GL_CHECK();
            std::cerr << "Error linking program:\n"
                      << infoLog.data();
            glDeleteProgram(program_id_);
            OM_GL_CHECK();
            return 0;
        }
        return program_id_;
    }

    GLuint vert_shader = 0;
    GLuint frag_shader = 0;
    GLuint program_id = 0;
};

static std::array<std::string_view, 17> event_names = {
    { /// input events
        "left_pressed", "left_released", "right_pressed", "right_released",
        "up_pressed", "up_released", "down_pressed", "down_released",
        "select_pressed", "select_released", "start_pressed", "start_released",
        "button1_pressed", "button1_released", "button2_pressed",
        "button2_released",
        /// virtual console events
        "turn_off" }
};

struct bind {
    bind(SDL_Keycode k, std::string_view s, event pressed, event released)
        : key(k)
        , name(s)
        , event_pressed(pressed)
        , event_released(released)
    {
    }

    SDL_Keycode key;
    std::string_view name;
    event event_pressed;
    event event_released;
};

const std::array<bind, 8> keys{
    { { SDLK_w, "up", event::up_pressed, event::up_released },
        { SDLK_a, "left", event::left_pressed, event::left_released },
        { SDLK_s, "down", event::down_pressed, event::down_released },
        { SDLK_d, "right", event::right_pressed, event::right_released },
        { SDLK_LCTRL, "button1", event::button1_pressed, event::button1_released },
        { SDLK_SPACE, "button2", event::button2_pressed, event::button2_released },
        { SDLK_ESCAPE, "select", event::select_pressed, event::select_released },
        { SDLK_RETURN, "start", event::start_pressed, event::start_released } }
};

vec2::vec2()
    : x(0.f)
    , y(1.f)
{
}

vec2::vec2(float x_, float y_)
    : x(x_)
    , y(y_)
{
}

vec2 operator+(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = l.x + r.x;
    result.y = l.y + r.y;
    return result;
}

mat2::mat2()
    : col0(1.0f, 0.f)
    , col1(0.f, 1.f)
{
}

mat2 mat2::identiry()
{
    return mat2::scale(1.f);
}

mat2 mat2::scale(float scale)
{
    mat2 result;
    result.col0.x = scale;
    result.col1.y = scale;
    return result;
}

mat2 mat2::rotation(float thetha)
{
    mat2 result;

    result.col0.x = std::cos(thetha);
    result.col0.y = std::sin(thetha);

    result.col1.x = -std::sin(thetha);
    result.col1.y = std::cos(thetha);

    return result;
}

vec2 operator*(const vec2& v, const mat2& m)
{
    vec2 result;
    result.x = v.x * m.col0.x + v.y * m.col0.y;
    result.y = v.x * m.col1.x + v.y * m.col1.y;
    return result;
}

mat2 operator*(const mat2& m1, const mat2& m2)
{
    mat2 r;

    r.col0.x = m1.col0.x * m2.col0.x + m1.col1.x * m2.col0.y;
    r.col1.x = m1.col0.x * m2.col1.x + m1.col1.x * m2.col1.y;
    r.col0.y = m1.col0.y * m2.col0.x + m1.col1.y * m2.col0.y;
    r.col1.y = m1.col0.y * m2.col1.x + m1.col1.y * m2.col1.y;

    return r;
}

tri0::tri0()
    : v{ v0(), v0(), v0() }
{
}

tri1::tri1()
    : v{ v1(), v1(), v1() }
{
}

tri2::tri2()
    : v{ v2(), v2(), v2() }
{
}

std::ostream& operator<<(std::ostream& stream, const event e)
{
    std::uint32_t value = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(event::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_names[value];
        return stream;
    } else {
        throw std::runtime_error("too big event value");
    }
}

std::istream& operator>>(std::istream& is, vertex& v)
{
    is >> v.x;
    is >> v.y;

    is >> v.tx;
    is >> v.ty;

    return is;
}

std::istream& operator>>(std::istream& is, triangle& t)
{
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, uv_pos& uv)
{
    is >> uv.u;
    is >> uv.v;
    return is;
}

std::istream& operator>>(std::istream& is, color& c)
{
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    float a = 0.f;
    is >> r;
    is >> g;
    is >> b;
    is >> a;
    c = color(r, g, b, a);
    return is;
}

std::istream& operator>>(std::istream& is, v0& v)
{
    is >> v.pos.x;
    is >> v.pos.y;

    return is;
}

std::istream& operator>>(std::istream& is, v1& v)
{
    is >> v.pos.x;
    is >> v.pos.y;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, v2& v)
{
    is >> v.pos.x;
    is >> v.pos.y;
    is >> v.uv.x;
    is >> v.uv.y;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, tri0& t)
{
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, tri1& t)
{
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, tri2& t)
{
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

/*
v0 blend(const v0& vl, const v0& vr, const float a)
{
    v0 r;
    r.pos.x = (1.0f - a) * vl.pos.x + a * vr.pos.x;
    r.pos.y = (1.0f - a) * vl.pos.y + a * vr.pos.y;
    return r;
}

tri0 blend(const tri0& tl, const tri0& tr, const float a)
{
    tri0 r;
    r.v[0] = blend(tl.v[0], tr.v[0], a);
    r.v[1] = blend(tl.v[1], tr.v[1], a);
    r.v[2] = blend(tl.v[2], tr.v[2], a);
    return r;
}
*/

///////////////////////////////////////////////////////////////////////////////
bool check_input(const SDL_Event& e, const bind*& result)
{
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind& b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief The EngineReal class
///
class EngineReal : public Engine {
    SDL_Window* window = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint program_id_ = 0;

    //const
    shader_gl_es20* shader00 = nullptr;
    shader_gl_es20* shader01 = nullptr;
    shader_gl_es20* shader02 = nullptr;

public:
    std::string initialize();
    float getTimeFromInit();
    bool loadTexture(std::string_view);
    void start();
    bool input(event& e);
    void renderTriangle(const triangle&);

    texture* createTexture(std::string_view path);
    void destroyTexture(texture* t);
    void render(const tri0&, const color&);
    void render(const tri1&);
    void render(const tri2&, texture*);

    void swapBuffers();
    void uninitialize();
    ~EngineReal() {}
};

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::initialize
/// \return
///
std::string EngineReal::initialize()
{
    using namespace std;

    stringstream serr;

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: initialization failed: " << err_message << std::endl;
    }

    /*SDL_Window* const */
    window = SDL_CreateWindow("Window on SDL with OpenGL, Keys and Triangle",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640,
        480, ::SDL_WINDOW_OPENGL);
    if (window == nullptr) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: Create Window failed: " << err_message << std::endl;
        SDL_Quit();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// \brief version GL
    ///
    int gl_major_ver = 2;
    int gl_minor_ver = 0;
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    //create context
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("can't create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        cout << serr.str();
    }

    int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    if (gl_major_ver < 2) {
        std::clog << "current context opengl version: " << gl_major_ver << '.'
                  << gl_minor_ver << '\n'
                  << "need openg version at least: 2.1\n"
                  << std::flush;
        throw std::runtime_error("opengl version too low");
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glDisableVertexAttribArray", glDisableVertexAttribArray);
        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniform1i", glUniform1i);
        load_gl_func("glActiveTexture", glActiveTextureMY);
        load_gl_func("glUniform4fv", glUniform4fv);
    } catch (std::exception& ex) {
        return ex.what();
    }

    // clang-format off
    shader00 = new shader_gl_es20(
        R"(
                                  precision mediump float;
                                  attribute vec2 a_position;
                                  void main()
                                  {
                                    gl_Position = vec4(a_position, 0.0, 1.0);
                                  }
                                  )",
        R"(
                                  precision mediump float;
                                  uniform vec4 u_color;
                                  void main()
                                  {
                                    gl_FragColor = u_color;
                                  }
                                  )",
                                  { { 0, "a_position" } });

    shader00->use();
    shader00->set_uniform("u_color", color(1.f, 0.f, 0.f, 1.f));

    shader01 = new shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec4 a_color;
                varying vec4 v_color;
                void main()
                {
                    v_color = a_color;
                    gl_Position = vec4(a_position, 0.0, 1.0);
                }
                )",
        R"(
                precision mediump float;
                varying vec4 v_color;
                void main()
                {
                    gl_FragColor = v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" } });

    shader01->use();

    shader02 = new shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec2 a_tex_coord;
                attribute vec4 a_color;
                varying vec4 v_color;
                varying vec2 v_tex_coord;
                void main()
                {
                    v_tex_coord = a_tex_coord;
                    v_color = a_color;
                    gl_Position = vec4(a_position, 0.0, 1.0);
                }
                )",
        R"(
                precision mediump float;
                varying vec2 v_tex_coord;
                varying vec4 v_color;
                uniform sampler2D s_texture;
                void main()
                {
                    gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

                // clang-format on

                // turn on rendering with just created shader program
                shader02->use();

                glEnable(GL_BLEND);
                OM_GL_CHECK();
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                OM_GL_CHECK();

                glClearColor(0.f, 0.0, 0.f, 0.0f);
                OM_GL_CHECK();

                glViewport(0, 0, 640, 480);
                OM_GL_CHECK();

                return "";
                // clang-format on
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::input
///
bool EngineReal::input(event& e)
{
    using namespace std;
    while (true) {
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) { //waiting event
            const bind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT) {
                e = grottans::event::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_pressed; //????????????????????????????
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_released; //????????????????????????????
                    return true;
                }
            }
        }
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::start
///
void EngineReal::start()
{
    /*
    texture* texture = this->createTexture("tank.png");
    if (nullptr == texture) {
        std::cerr << "failed load texture\n";
        std::cout << "failed load texture\n";
    }

    bool continue_loop = true;
    int current_shader = 0;
    while (continue_loop) {
        event e;
        while (input(e)) {
            std::cout << e << std::endl;
            switch (e) {
            case grottans::event::turn_off:
                continue_loop = false;
                break;
            case grottans::event::button1_released:
                ++current_shader;
                if (current_shader > 2) {
                    current_shader = 0;
                }
                break;
            default:
                break;
            }
        }

        if (current_shader == 0) {
            std::ifstream file("vert_pos.txt");
            assert(!!file);

            tri0 tr1;
            tri0 tr2;
            tri0 tr11;
            tri0 tr22;

            file >> tr1 >> tr2 >> tr11 >> tr22;

            float time = this->getTimeFromInit();
            float alpha = std::sin(time);

            tri0 t1 = blend(tr1, tr11, alpha);
            tri0 t2 = blend(tr2, tr22, alpha);

            this->render(t1, color(1.f, 0.f, 0.f, 1.f));
            this->render(t2, color(0.f, 1.f, 0.f, 1.f));
        }

        if (current_shader == 1) {
            std::ifstream file("vert_pos_color.txt");
            assert(!!file);

            tri1 tr1;
            tri1 tr2;

            file >> tr1 >> tr2;

            this->render(tr1);
            this->render(tr2);
        }

        if (current_shader == 2) {
            std::ifstream file("vert_tex_color.txt");
            assert(!!file);

            tri2 tr1;
            tri2 tr2;

            file >> tr1 >> tr2;

            float time = this->getTimeFromInit();
            float s = std::sin(time);
            float c = std::cos(time);

            // animate one triangle texture coordinates
            for (auto& v : tr1.v) {
                v.uv.u += c;
                v.uv.v += s;
            }

            this->render(tr1, texture);
            this->render(tr2, texture);
        }

        this->swapBuffers();
    }
    */
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::loadTexture
///
bool EngineReal::loadTexture(std::string_view path)
{
    std::vector<unsigned char> png_file_in_memory;
    std::ifstream ifs(path.data(), std::ios_base::binary);
    if (!ifs) {
        return false;
    }
    ifs.seekg(0, std::ios_base::end);
    size_t pos_in_file = static_cast<size_t>(ifs.tellg());
    png_file_in_memory.resize(pos_in_file);
    ifs.seekg(0, std::ios_base::beg);
    if (!ifs) {
        return false;
    }

    ifs.read(reinterpret_cast<char*>(png_file_in_memory.data()), pos_in_file);
    if (!ifs.good()) {
        return false;
    }

    std::vector<unsigned char> image; //keep png
    unsigned long w = 0;
    unsigned long h = 0;
    int error = decodePNG(image, w, h, &png_file_in_memory[0],
        png_file_in_memory.size(), false);

    // if there's an error, display it
    if (error != 0) {
        std::cerr << "error: " << error << std::endl;
        return false;
    }

    GLuint tex_handl = 0;
    glGenTextures(1, &tex_handl);
    OM_GL_CHECK();
    glBindTexture(GL_TEXTURE_2D, tex_handl);
    OM_GL_CHECK();

    GLint mipmap_level = 0;
    GLint border = 0;
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, w, h, border,
        GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
    OM_GL_CHECK();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    OM_GL_CHECK();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    OM_GL_CHECK();
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::renderTriangle
///
void EngineReal::renderTriangle(const triangle& t)
{
    // vertex coordinates
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), &t.v[0].x);
    OM_GL_CHECK();

    glEnableVertexAttribArray(0);
    OM_GL_CHECK();

    // texture coordinates
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), &t.v[0].tx);
    OM_GL_CHECK();

    glEnableVertexAttribArray(1);
    OM_GL_CHECK();

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::render
///
void EngineReal::render(const tri0& t, const color& c)
{
    shader00->use();
    shader00->set_uniform("u_color", c);
    // vertex coordinates
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(v0),
        &t.v[0].pos.x);
    OM_GL_CHECK();
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::render
///
void EngineReal::render(const tri1& t)
{
    shader01->use();
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]), &t.v[0].pos);
    OM_GL_CHECK();
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();
    // colors
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(t.v[0]), &t.v[0].c);
    OM_GL_CHECK();
    glEnableVertexAttribArray(1);
    OM_GL_CHECK();

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();

    glDisableVertexAttribArray(1);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief render
///
void EngineReal::render(const tri2& t, texture* tex)
{
    shader02->use();
    texture_gl_es20* texture = static_cast<texture_gl_es20*>(tex);
    texture->bind();
    shader02->set_uniform("s_texture", texture);
    // positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]),
        &t.v[0].pos);
    OM_GL_CHECK();
    glEnableVertexAttribArray(0);
    OM_GL_CHECK();
    // colors
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(t.v[0]),
        &t.v[0].c);
    OM_GL_CHECK();
    glEnableVertexAttribArray(1);
    OM_GL_CHECK();

    // texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(t.v[0]),
        &t.v[0].uv);
    OM_GL_CHECK();
    glEnableVertexAttribArray(2);
    OM_GL_CHECK();

    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK();

    glDisableVertexAttribArray(1);
    OM_GL_CHECK();
    glDisableVertexAttribArray(2);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief color::color
///
color::color(std::uint32_t rgba_)
    : rgba(rgba_)
{
}

color::color(float r, float g, float b, float a)
{
    assert(r <= 1 && r >= 0);
    assert(g <= 1 && g >= 0);
    assert(b <= 1 && b >= 0);
    assert(a <= 1 && a >= 0);

    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);

    rgba = a_ << 24 | b_ << 16 | g_ << 8 | r_;
}

float color::get_r() const
{
    std::uint32_t r_ = (rgba & 0x000000FF) >> 0;
    return r_ / 255.f;
}
float color::get_g() const
{
    std::uint32_t g_ = (rgba & 0x0000FF00) >> 8;
    return g_ / 255.f;
}
float color::get_b() const
{
    std::uint32_t b_ = (rgba & 0x00FF0000) >> 16;
    return b_ / 255.f;
}
float color::get_a() const
{
    std::uint32_t a_ = (rgba & 0xFF000000) >> 24;
    return a_ / 255.f;
}

void color::set_r(const float r)
{
    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (r_ << 0);
}
void color::set_g(const float g)
{
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (g_ << 8);
}
void color::set_b(const float b)
{
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (b_ << 16);
}
void color::set_a(const float a)
{
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);
    rgba &= 0x00FFFFFF;
    rgba |= a_ << 24;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief texture_gl_es20::texture_gl_es20
///
texture_gl_es20::texture_gl_es20(std::string_view path)
    : file_path(path)
{
    std::vector<unsigned char> png_file_in_memory;
    std::ifstream ifs(path.data(), std::ios_base::binary);
    if (!ifs) {
        throw std::runtime_error("can't load texture");
    }
    ifs.seekg(0, std::ios_base::end);
    std::streamoff pos_in_file = ifs.tellg();
    png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
    ifs.seekg(0, std::ios_base::beg);
    if (!ifs) {
        throw std::runtime_error("can't load texture");
    }

    ifs.read(reinterpret_cast<char*>(png_file_in_memory.data()), pos_in_file);
    if (!ifs.good()) {
        throw std::runtime_error("can't load texture");
    }

    std::vector<unsigned char> image;
    unsigned long w = 0;
    unsigned long h = 0;
    int error = decodePNG(image, w, h, &png_file_in_memory[0],
        png_file_in_memory.size(), false);

    // if there's an error, display it
    if (error != 0) {
        std::cerr << "error: " << error << std::endl;
        throw std::runtime_error("can't load texture");
    }

    glGenTextures(1, &tex_handl);
    OM_GL_CHECK();
    glBindTexture(GL_TEXTURE_2D, tex_handl);
    OM_GL_CHECK();

    GLint mipmap_level = 0;
    GLint border = 0;
    GLsizei width_ = static_cast<GLsizei>(w);
    GLsizei height_ = static_cast<GLsizei>(h);
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width_, height_, border,
        GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
    OM_GL_CHECK();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    OM_GL_CHECK();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    OM_GL_CHECK();
}

texture_gl_es20::~texture_gl_es20()
{
    glDeleteTextures(1, &tex_handl);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::getTimeFromInit
/// \return
///
float EngineReal::getTimeFromInit()
{
    std::uint32_t ms_from_library_initialization = SDL_GetTicks();
    float seconds = ms_from_library_initialization * 0.001f;
    return seconds;
}

///////////////////////////////////////////////////////////////////////////////
/// \brief EngineReal::createTexture
///
texture* EngineReal::createTexture(std::string_view path)
{
    return new texture_gl_es20(path);
}

void EngineReal::destroyTexture(texture* t) { delete t; }

///////////////////////////////////////////////////////////////////////////////
/// \brief swap_buffers
///
void EngineReal::swapBuffers()
{
    SDL_GL_SwapWindow(window);

    glClear(GL_COLOR_BUFFER_BIT);
    OM_GL_CHECK();
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Engine::uninitialize
///
void EngineReal::uninitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
    destroy_engine(this);
}

///////////////////////////////////////////////////////////////////////////////
static bool already_exist = false;

Engine* create_engine()
{
    if (already_exist) {
        throw std::runtime_error("engine already exist");
    }
    Engine* result = new EngineReal();
    already_exist = true;
    return result;
}

void destroy_engine(Engine* e)
{
    if (already_exist == false) {
        throw std::runtime_error("engine not created");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr");
    }
    delete e;
}

} // end of namespace grottans
