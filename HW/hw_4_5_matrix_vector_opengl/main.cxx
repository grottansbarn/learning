#include "engine.hxx"
#include <cassert>
#include <fstream>
#include <iostream>
#include <math.h>

grottans::v0 blend(const grottans::v0& vl, const grottans::v0& vr, const float a)
{
    grottans::v0 r;
    r.pos.x = (1.0f - a) * vl.pos.x + a * vr.pos.x;
    r.pos.y = (1.0f - a) * vl.pos.y + a * vr.pos.y;
    return r;
}

grottans::tri0 blend(const grottans::tri0& tl, const grottans::tri0& tr, const float a)
{
    grottans::tri0 r;
    r.v[0] = blend(tl.v[0], tr.v[0], a);
    r.v[1] = blend(tl.v[1], tr.v[1], a);
    r.v[2] = blend(tl.v[2], tr.v[2], a);
    return r;
}

int main()
{
    grottans::Engine* enginereal = grottans::create_engine();

    enginereal->initialize();

    //enginereal->start();
    grottans::texture* texture = enginereal->createTexture("tank.png");
    if (nullptr == texture) {
        std::cerr << "failed load texture\n";
    }

    bool continue_loop = true;
    int current_shader = 0;
    while (continue_loop) {
        grottans::event e;
        while (enginereal->input(e)) {
            std::cout << e << std::endl;
            switch (e) {
            case grottans::event::turn_off:
                continue_loop = false;
                break;
            case grottans::event::button1_released:
                ++current_shader;
                if (current_shader > 2) {
                    current_shader = 0;
                }
                break;
            default:
                break;
            }
        }

        if (current_shader == 0) {
            std::ifstream file("vert_pos.txt");
            assert(!!file);

            grottans::tri0 tr1;
            grottans::tri0 tr2;
            grottans::tri0 tr11;
            grottans::tri0 tr22;

            file >> tr1 >> tr2 >> tr11 >> tr22;

            float time = enginereal->getTimeFromInit();
            float alpha = std::sin(time);

            grottans::tri0 t1 = blend(tr1, tr11, alpha);
            grottans::tri0 t2 = blend(tr2, tr22, alpha);

            enginereal->render(t1, grottans::color(1.f, 0.f, 0.f, 1.f));
            enginereal->render(t2, grottans::color(0.f, 1.f, 0.f, 1.f));
        }

        if (current_shader == 1) {
            std::ifstream file("vert_pos_color.txt");
            assert(!!file);

            grottans::tri1 tr1;
            grottans::tri1 tr2;

            file >> tr1 >> tr2;

            enginereal->render(tr1);
            enginereal->render(tr2);
        }

        if (current_shader == 2) {
            std::ifstream file("vert_tex_color.txt");
            assert(!!file);

            grottans::tri2 tr1;
            grottans::tri2 tr2;

            file >> tr1 >> tr2;

            float time = enginereal->getTimeFromInit();
            float s = std::sin(time);
            float c = std::cos(time);

            // animate one triangle texture coordinates
            for (auto& v : tr1.v) {
                v.uv.x += c;
                v.uv.y += s;
            }

            enginereal->render(tr1, texture);

            grottans::mat2 aspect;
            aspect.col0.x = 640.f / 480.f;
            aspect.col0.y = 0.f;
            aspect.col1.x = 0.f;
            aspect.col1.y = 1.f;

            grottans::mat2 m = aspect * grottans::mat2::rotation(time) * grottans::mat2::scale(std::sin(time));

            for (auto& v : tr2.v) {
                v.pos = v.pos * m;
            }

            enginereal->render(tr2, texture);
        }

        enginereal->swapBuffers();
    }

    enginereal->uninitialize();

    return EXIT_SUCCESS;
}
